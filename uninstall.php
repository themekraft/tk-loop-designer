<?php

// Make sure that we are uninstalling
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit();
}

// Removes all data from the database
delete_option( 'tk_loop_designer_license_manager' );
delete_option( 'tk_loop_designer_product_id');
// delete_option( 'tk_loop_designer_instance' );
delete_option( 'tk_loop_designer_deactivate_checkbox' );
delete_option( 'tk_loop_designer_activated' );
delete_option( 'tk_loop_designer_version' );
