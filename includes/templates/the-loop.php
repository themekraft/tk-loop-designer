<?php global $wp_query, $the_lp_query;

if ( !$the_lp_query ) {
	$the_lp_query = $wp_query;
}

do_action('tk_loop_designer_before_loop');

if ( $the_lp_query->have_posts() ) :
	while ( $the_lp_query->have_posts() ) :
		$the_lp_query->the_post();

		tk_loop_designer_locate_template( '/templates/the-loop-item.php' );

	endwhile;
endif;

remove_all_actions('tk_loop_designer_before_loop_item');
//remove_action( 'tk_loop_designer_before_loop_item','tk_loop_designer_list_posts_featured_image' );

do_action('tk_loop_designer_after_loop');
?>