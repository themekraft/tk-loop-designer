<?php global $list_post_atts; ?>

<?php extract( $list_post_atts ); ?>

<?php do_action('tk_loop_designer_before_loop_item'); ?>

<?php if ( $clickable == 'on' ) { ?>
	<a class="clickable" href="<?php the_permalink() ?>" target="<?php echo $link_target; ?>" title="<?php the_title_attribute(); ?>">
<?php } ?>

<div class="listposts <?php echo $img_position .' '. $template_name .' '. do_action('tk_loop_designer_listposts_class'); ?>">

	<?php do_action('tk_loop_designer_before_entry_inner'); ?>

	<div class="ld-entry-inner <?php do_action('tk_loop_designer_entry_inner_class'); ?>">

		<?php do_action('tk_loop_designer_before_loop_title'); ?>

		<?php if ( $show_title == 'on' ) : ?>

			<?php if( $clickable == 'on'){ ?>
				<h3 class="ld-post-title"><span class="link"><?php the_title() ?></span></h3>
			<?php } else { ?>
				<h3 class="ld-post-title"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" target="<?php echo $link_target; ?>"><?php the_title() ?></a></h3>
			<?php } ?>

		<?php endif; ?>

		<?php do_action('tk_loop_designer_after_loop_title'); ?>

	</div>

	<?php do_action('tk_loop_designer_after_entry_inner'); ?>

</div>

<?php if ( $clickable == 'on') { ?>
	</a>
<?php } ?>

<?php do_action('tk_loop_designer_after_loop_item'); ?>