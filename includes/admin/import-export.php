<?php

function tk_loop_designer_import_export_screen(){
	tk_loop_designer_import_export_control();
	$tk_loop_designer = get_option('tk_loop_designer_options');

	?>
	<div class="wrap">
		<div id="icon-options-general" class="icon32"><br></div>
		<h2><?php _e( 'Manage Template', 'tk-loop-designer' ); ?></h2>

		<span style="font-size: 13px; float:right;"><?php _e( 'Proudly brought to you by', 'tk-loop-designer' ); ?> <a href="http://themekraft.com/" target="_new">Themekraft</a>.</span>

		<div class="message updated below-h2" style="margin: 30px 0 0 0; background: #f4f4f4; padding: 20px; overflow: auto; border-radius: 6px;">


			<div style="float: left; overflow: auto; border-right: 1px solid #ddd; padding: 0 20px 0 0;">
				<h3><?php _e( 'Get Support.', 'tk-loop-designer' ); ?></h3>
				<p><a class="button secondary" href="https://themekraft.zendesk.com/hc/en-us/categories/200002792-TK-Loop-Designer" target="_new">Documentation</a> <a class="button secondary" onClick="script: Zenbox.show(); return false;" class="button secondary"  href="#" target="_blank" title="Submit an email support ticket"><?php _e( 'Ask Question', 'tk-loop-designer' ); ?></a></p>
			</div>

			<div style="float: left; overflow: auto; padding: 0 20px 0 20px; border-right: 1px solid #ddd;">
				<h3><?php _e( 'Contribute your ideas.', 'tk-loop-designer' ); ?></h3>
				<p><?php _e( 'Add ideas and vote in our', 'tk-loop-designer' ); ?> <a class="button button-secondary" href="https://themekraft.zendesk.com/hc/communities/public/topics/200001412-Loop-Designer-Ideas" target="_new"><?php _e( 'Ideas Forums', 'tk-loop-designer' ); ?></a></p>
			</div>


		</div>
		<br>

		<div id="post-body">
			<div id="post-body-content">
				<?php tk_loop_designer_import_export(); ?>
			</div>
		</div>
	</div>

<?php
}

function tk_loop_designer_import_export() {



	$tk_loop_designer = get_option('tk_loop_designer_options');

//	echo '<pre>';
//	print_r($tk_loop_designer);
//	echo '</pre>';

	if(!isset($tk_loop_designer['templates'])){
		printf( '<p>%s</p>', __('No Templates available to export', 'tk-loop-designer' ) );
		return;
	}
    ?>

    <h2><?php _e('Manage Templates', 'tk-loop-designer')?></h2>
    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<p class="submit alignleft">
			<input id="tk_loop_designer_export" name="tk_loop_designer_export" type="button" class="button button-secondary" value="<?php _e('Export Selected Templates','tk-loop-designer');?>" />
		</p>
		<p class="submit alignleft">
			&nbsp;<input id="tk_loop_designer_delete" name="tk_loop_designer_delete" type="button" class="button button-secondary" value="<?php _e('Delete Selected Templates','tk-loop-designer');?>" />
		</p>
		<p class="submit alignleft">
			&nbsp;<input id="tk_loop_designer_clone" name="tk_loop_designer_clone" type="button" class="button button-secondary" value="<?php _e('Clone Selected Templates','tk-loop-designer');?>" />
		</p>
		<p class="submit alignleft">
			&nbsp;<input id="tk_loop_designer_rescan" name="tk_loop_designer_rescan" type="button" class="button button-secondary" value="<?php _e('Rescan Template Folder','tk_loop_designer');?>" />
		</p>

		<?php tk_loop_designer_list_templates($tk_loop_designer['templates']); ?>

	</form>
	<?php
	if (isset($_POST['action']) && $_POST['action'] == 'Import') { ?>
	<h2><?php _e('Select Templates you want to Import', 'tk-loop-designer')?></h2>
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<?php
		$tk_loop_designer_options_import_tmp	= get_option('tk_loop_designer_options_import_tmp');

		if(isset($tk_loop_designer_options_import_tmp['templates']))
			tk_loop_designer_list_templates($tk_loop_designer_options_import_tmp['templates']);
		?>
		<p class="submit alignleft">
			<input name="action" type="submit" value="<?php _e('Import selected templates','tk-loop-designer');?>" />
		</p>

	</form>
<?php } else { ?>

	<h2><?php _e('Import Templates ', 'tk-loop-designer')?></h2>
	<form enctype="multipart/form-data" method="post">
		<p class="submit alignleft">
			<input type="file" name="file" />
		</p>
		<p class="submit alignleft">
			<input name="action" type="submit" class="button button-secondary" value="<?php _e('Import','tk-loop-designer');?>" />
		</p>

	</form>

<?php }

}

function tk_loop_designer_list_templates($tk_loop_designer) { ?>

	<style type="text/css">
	 .tk-loop-designer-editinline{
	 	color: #bc0b0b;
	 	cursor: pointer;
	 }
		table #the-list tr .tk-loop-designer-row-actions { opacity:0 }
		table #the-list tr:hover .tk-loop-designer-row-actions { opacity:1 }

    </style>
    <div id="chnage_template_name" style="display:none"></div>
	<table class="wp-list-table widefat fixed posts">
	<thead>
		<tr>
			<th scope="col" id="cb" class="manage-column column-cb check-column" style="">
				<label class="screen-reader-text" for="cb-select-all-1"><?php _e( 'Select All', 'tk-loop-designer' ); ?></label>
				<input id="cb-select-all-1" type="checkbox">
			</th>
			<th scope="col" id="name" class="manage-column column-comment sortable desc" style=""><?php _e( 'Name', 'tk-loop-designer' ); ?></th>
			<th scope="col" id="slug" class="manage-column column-comment sortable desc" style=""><?php _e( 'Slug', 'tk-loop-designer' ); ?></th>
	</thead>
	<tbody id="the-list">
	<?php foreach ($tk_loop_designer as $key => $template) {?>
		<tr>
			<th scope="row" class="check-column">
				<label class="screen-reader-text" for="aid-<?php echo $key ?>"><?php $key; ?></label>
				<input type="checkbox" name="tk_loop_designer_template_slugs[]" value="<?php echo $key ?>" id="aid-<?php echo $key ?>">
			</th>
			<td class="slug column-slug">
				<?php echo isset($template) ? $template: '--'; ?>
				<div class="tk-loop-designer-row-actions">
					<span class="tk-loop-designer-inline hide-if-no-js">
						<input id="<?php echo $key ?>" alt="#TB_inline?height=300&amp;width=400&amp;inlineId=chnage_template_name" title="Change template name" class="thickbox_edit tk-loop-designer-editinline tk-loop-designer-thickbox" type="button" value="Rename" />
					</span>
				</div>
			</td>
			<td class="slug column-slug">
				<?php echo isset($key) ? $key: '--'; ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
	</table>
<?php }


function tk_loop_designer_import_export_control(){



	if (isset($_POST['action'])) {

		$options = get_option('tk_loop_designer_options');
		$action = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : '';
		switch ( $action ) {
			case 'Export':
				$method = 'Export';
				$done = 'cap_serialize_export';
				header('Content-Type: text/csv; charset=utf-8');
				header('Content-Disposition: attachment; filename=loop-designer-export.txt');
				// create a file pointer connected to the output stream
				echo serialize( $options );
				exit();
				break;
			case 'Import':
				$method = 'Import';
					if(empty($_FILES['file']['tmp_name']))
						return;
					$data = unserialize( implode ('', file ($_FILES['file']['tmp_name'])));
					update_option("tk_loop_designer_options_import_tmp", $data);
				break;
			case 'Import selected templates':

					if(!isset($_POST['tk_loop_designer_template_slugs']))
						return;

					$tk_loop_designer_template_slugs = $_POST['tk_loop_designer_template_slugs'];
					tk_loop_designer_import_templates($tk_loop_designer_template_slugs);

				break;
			case 'Reset to default':
				$method = 'remove-all';
					delete_option("tk_loop_designer_loop_designer_options");
			break;
			}
	}

}

function tk_loop_designer_import_templates($tk_loop_designer_template_slugs){


	$tk_loop_designer_options				= get_option('tk_loop_designer_options');
	$tk_loop_designer_options_import_tmp	= get_option('tk_loop_designer_options_import_tmp');

	foreach ($tk_loop_designer_template_slugs as $template) {

		if(isset($tk_loop_designer_options['templates'][$template]) && isset($tk_loop_designer_options_import_tmp['templates'][$template])){
			$md5_tmp = substr(md5(time() * rand()), 0, 5);
			$template_slug = 'Copy-of-'. $template . '-' . $md5_tmp;
			$template_name = 'Copy of '. $tk_loop_designer_options_import_tmp['templates'][$template] . '-' . $md5_tmp;
		} else {
			$template_slug = $template;
			$template_name = $tk_loop_designer_options_import_tmp['templates'][$template];
		}


		$tk_loop_designer_options['templates'][$template_slug]			= $template_name;
		$tk_loop_designer_options['template_options'][$template_slug]	= $tk_loop_designer_options_import_tmp['template_options'][$template];

	}
	update_option("tk_loop_designer_options", $tk_loop_designer_options);

}


function tk_loop_designer_import(){
	$tk_loop_designer	= get_option('tk_loop_designer_options');
	$tk_loop_designer_import_forms	= $_POST['tk_loop_designer_import_forms'];
	//$data = unserialize($tk_loop_designer_import_forms);

	//print_r($data);
	$tk_loop_designer_import_forms = str_replace("/", "", $tk_loop_designer_import_forms);
	echo $tk_loop_designer_import_forms;//unserialize(trim($tk_loop_designer_import_forms));

	die();
}
add_action( 'wp_ajax_tk_loop_designer_import', 'tk_loop_designer_import' );
add_action( 'wp_ajax_nopriv_tk_loop_designer_import', 'tk_loop_designer_import' );

function tk_loop_designer_export(){

	$tk_loop_designer_options				= get_option('tk_loop_designer_options');
	$tk_loop_designer_template_slugs		= $_POST['tk_loop_designer_template_slugs'];
	$tk_loop_designer_export_tmp			= Array();

	foreach ( $tk_loop_designer_template_slugs as $template_slug) {

		if($template_slug != 'on'){

			if(isset($tk_loop_designer_options['templates'][$template_slug]))
				$tk_loop_designer_export_tmp['templates'][$template_slug] = $tk_loop_designer_options['templates'][$template_slug];

			if(isset($tk_loop_designer_options['template_options'][$template_slug]))
				$tk_loop_designer_export_tmp['template_options'][$template_slug] = $tk_loop_designer_options['template_options'][$template_slug];

		}

	}

	echo serialize( $tk_loop_designer_export_tmp );

	die();
}
add_action( 'wp_ajax_tk_loop_designer_export', 'tk_loop_designer_export' );
add_action( 'wp_ajax_nopriv_tk_loop_designer_export', 'tk_loop_designer_export' );

function tk_loop_designer_clone(){

	$template_slugs	= $_POST['template_slugs'];

	if(!is_array($template_slugs))
		return;

	$tk_loop_designer_options				= get_option('tk_loop_designer_options');

	foreach ($template_slugs as $template) {

		if(isset($tk_loop_designer_options['templates'][$template])){
			$md5_tmp = substr(md5(time() * rand()), 0, 5);
			$template_slug = 'Clone-of-'. $template . '-' . $md5_tmp;
			$template_name = 'Clone of '. $tk_loop_designer_options['templates'][$template] . '-' . $md5_tmp;
		}

		$tk_loop_designer_options['templates'][$template_slug]			= $template_name;
		$tk_loop_designer_options['template_options'][$template_slug]	= $tk_loop_designer_options['template_options'][$template];

	}
	update_option("tk_loop_designer_options", $tk_loop_designer_options);

	die();
}
add_action( 'wp_ajax_tk_loop_designer_clone', 'tk_loop_designer_clone' );
add_action( 'wp_ajax_nopriv_tk_loop_designer_clone', 'tk_loop_designer_clone' );

function tk_loop_designer_rescan(){

			$tk_loop_designer = get_option('tk_loop_designer_options');
			$loop_templates_directory = array_diff(scandir(LOOP_DESIGNER_LOOP_TEMPLATES_PATH), array('..', '.'));

			foreach ($loop_templates_directory as $key => $template) {

				$tk_loop_designer_templates = unserialize( file_get_contents(LOOP_DESIGNER_LOOP_TEMPLATES_PATH . $template, true));

				foreach ($tk_loop_designer_templates['templates'] as $key => $template_options) {


					if(isset($tk_loop_designer['templates'][$key]) && isset($tk_loop_designer_templates['templates'][$key])){
						// $md5_tmp = substr(md5(time() * rand()), 0, 5);
						// $template_slug = 'Copy-of-'. $key . '-' . $md5_tmp;
						// $template_name = 'Copy of '. $tk_loop_designer_templates['templates'][$key] . '-' . $md5_tmp;
					} else {
						$template_slug = $key;
						$template_name = $tk_loop_designer_templates['templates'][$key];
					}

					if(isset($template_slug)){
						$tk_loop_designer['templates'][$template_slug]			= $template_name;
						$tk_loop_designer['template_options'][$template_slug]	= $tk_loop_designer_templates['template_options'][$template_slug];
					}


				}

			}
			update_option("tk_loop_designer_options", $tk_loop_designer);


	die();
}
add_action( 'wp_ajax_tk_loop_designer_rescan', 'tk_loop_designer_rescan' );
add_action( 'wp_ajax_nopriv_tk_loop_designer_rescan', 'tk_loop_designer_rescan' );

function tk_loop_designer_edit_template_name_save(){
	$tk_loop_designer_options = get_option('tk_loop_designer_options');

	if(isset($_POST['tk_loop_designer_template_name']))
		$tk_loop_designer_template_name = $_POST['tk_loop_designer_template_name'];

	if(isset($_POST['tk_loop_designer_template_slug']))
		$tk_loop_designer_template_slug = $_POST['tk_loop_designer_template_slug'];


	$tk_loop_designer_options['templates'][sanitize_title($tk_loop_designer_template_name)]			= $tk_loop_designer_template_name;
	$tk_loop_designer_options['template_options'][sanitize_title($tk_loop_designer_template_name)]	= $tk_loop_designer_options['template_options'][$tk_loop_designer_template_slug];

	unset( $tk_loop_designer_options['templates'][$tk_loop_designer_template_slug] );
	unset( $tk_loop_designer_options['template_options'][$tk_loop_designer_template_slug] );

	update_option("tk_loop_designer_options", $tk_loop_designer_options);

}
add_action( 'wp_ajax_tk_loop_designer_edit_template_name_save', 'tk_loop_designer_edit_template_name_save' );
add_action( 'wp_ajax_nopriv_tk_loop_designer_edit_template_name_save', 'tk_loop_designer_edit_template_name_save' );

function tk_loop_designer_edit_template_name(){

	$tk_loop_designer_options = get_option('tk_loop_designer_options');

	if(isset($_POST['tk_loop_designer_template_id']))
		$tk_loop_designer_template_id = $_POST['tk_loop_designer_template_id'];?>

	<p><b>Change the Template name</b><br>
	<input id='tk_loop_designer_template_name' name='tk_loop_designer_template_name' type='text' value='<?php echo $tk_loop_designer_options['templates'][$tk_loop_designer_template_id]; ?>' /></p>
	<input id='tk_loop_designer_template_slug' name='tk_loop_designer_template_slug' type='hidden' value='<?php echo $tk_loop_designer_template_id; ?>' /></p>

	<input type="button" value="Save" name="tk_loop_designer_edit_template" class="button tk_loop_designer_edit_template btn">
	<?php

}
add_action( 'wp_ajax_tk_loop_designer_edit_template_name', 'tk_loop_designer_edit_template_name' );
add_action( 'wp_ajax_nopriv_tk_loop_designer_edit_template_name', 'tk_loop_designer_edit_template_name' );