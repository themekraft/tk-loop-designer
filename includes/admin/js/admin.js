jQuery(document).ready(function(jQuery) {
	
	jQuery(".tk_loop_designer_edit_template").live("click", function() { 

		var action = jQuery(this);
		
		var tk_loop_designer_template_name	= jQuery( '#tk_loop_designer_template_name'	).val();
		var tk_loop_designer_template_slug	= jQuery( '#tk_loop_designer_template_slug'	).val();
		
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {"action": "tk_loop_designer_edit_template_name_save", "tk_loop_designer_template_name": tk_loop_designer_template_name, "tk_loop_designer_template_slug": tk_loop_designer_template_slug},
			success: function(data){
				window.location.reload(true);
			},
			error: function() { 
				alert('Something went wrong.. ;-(sorry)');
			}
		});

	});
	
	jQuery('.tk-loop-designer-editinline').click(function(){
		
		var action = jQuery(this);
		var tk_loop_designer_template_id = this.id;	
		var t = this.title || this.name || null;
	    var a = this.href || this.alt;
	    var g = this.rel || false;

		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			cache: false,
			data: {"action": "tk_loop_designer_edit_template_name", "tk_loop_designer_template_id": tk_loop_designer_template_id},
			success: function(data){
				jQuery("#chnage_template_name").html(data);
				tb_show(t,a,g);
			},
			error: function() { 
				alert('Something went wrong.. ;-(sorry)');
			}
		});
		
	});
	
	jQuery('#tk_loop_designer_rescan').click(function(){

		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {"action": "tk_loop_designer_rescan"},
			success: function(data){
				location.reload();
			},
			error: function() { 
				alert('Something went wrong.. ;-(sorry)');
			}
		});

	});

	jQuery('#tk_loop_designer_clone').click(function(){
		var template_slugs = [];
        jQuery(':checkbox:checked').each(function(i){
          template_slugs[i] = jQuery(this).val();
        });

		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {"action": "tk_loop_designer_clone", "template_slugs": template_slugs},
			success: function(data){
				location.reload();
			},
			error: function() { 
				alert('Something went wrong.. ;-(sorry)');
			}
		});

	});
	 
	jQuery('#tk_loop_designer_delete').click(function(){

		var template_name = [];
        jQuery(':checkbox:checked').each(function(i){
          template_name[i] = jQuery(this).val();
        });
		if (confirm('Delete Permanently'))
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {"action": "tk_loop_designer_delete_template", "template_name": template_name},
			success: function(data){
				location.reload();
			},
			error: function() { 
				alert('Something went wrong.. ;-(sorry)');
			}
		});

	});
		   
	jQuery('#tk_loop_designer_export').click(function(){

		var tk_loop_designer_template_slugs = [];
        jQuery(':checkbox:checked').each(function(i){
          tk_loop_designer_template_slugs[i] = jQuery(this).val();
        });

		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {"action": "tk_loop_designer_export", "tk_loop_designer_template_slugs": tk_loop_designer_template_slugs},
			success: function(data){
				var a = document.createElement('a');
				var blob = new Blob([data], {'type':'application\/octet-stream'});

				a.href = window.URL.createObjectURL(blob);
				a.download = 'tk_loop_designer_export.txt';
				a.click();
			},
			error: function() { 
				alert('Something went wrong.. ;-(sorry)');
			}
		});

	});

	if (typeof(Zenbox) !== "undefined") {
		Zenbox.init({
			dropboxID:   "20204992",
			url:         "https://themekraft.zendesk.com",
			tabTooltip:  "Feedback",
			tabColor:    "black",
			tabPosition: "Left",
			hide_tab: true
		});
	}

});