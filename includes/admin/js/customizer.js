// This file here handles the live changes in your preview. 
( function( $ ) {

	// Update the h1 titles in real time...
	wp.customize( 'test', function( value ) {
		value.bind( function( newval ) {
			if(newval == 'none'){
				$('h1, h1 a').css('font-family', '' );
				
			} else {
				$('h1, h1 a').css('font-family', newval );
			}
		} );
	} );
	
} )( jQuery );



