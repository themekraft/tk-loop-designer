<?php

/**
 * Adding the Admin Page
 *
 * @author Sven Lehnert
 * @package TK Loop Designer
 * @since 1.0
 */

add_action( 'admin_menu', 'tk_loop_designer_admin_menu' );

function tk_loop_designer_admin_menu() {
	add_menu_page( __( 'TK Loop Designer', 'tk-loop-designer' ), __( 'Loop Designer', 'tk-loop-designer' ), 'edit_theme_options', 'tk_loop_designer_options', 'tk_loop_designer_screen' );
	add_submenu_page( 'tk_loop_designer_options', __( 'Loop Manager', 'tk-loop-designer' ), __( 'Loop Manager', 'tk-loop-designer' ), 'edit_theme_options', 'tk_loop_designer_import-export', 'tk_loop_designer_import_export_screen' );

}

/**
 * The Admin Page
 *
 * @author Sven Lehnert
 * @package TK Loop Designer
 * @since 1.0
 */

function tk_loop_designer_screen() { ?>

    <div class="wrap">

        <div id="icon-options-general" class="icon32"><br></div>
        <h2><?php _e( 'Loop Designer Setup', 'tk-loop-designer' ); ?></h2>

			<span style="font-size: 13px; float:right;"><?php _e( 'Proudly brought to you by', 'tk-loop-designer' ); ?> <a href="http://themekraft.com/" target="_new">Themekraft</a>.</span>

        <div class="message updated below-h2" style="margin: 30px 0 0 0; background: #f4f4f4; padding: 20px; overflow: auto; border-radius: 6px;">


			<div style="float: left; overflow: auto; border-right: 1px solid #ddd; padding: 0 20px 0 0;">
				<h3><?php _e( 'Get Support.', 'tk-loop-designer' ); ?></h3>
				<p><a class="button secondary" href="https://themekraft.zendesk.com/hc/en-us/categories/200002792-TK-Loop-Designer" target="_new">Documentation</a> <a class="button secondary" onClick="script: Zenbox.show(); return false;" class="button secondary"  href="#" target="_blank" title="Submit an email support ticket"><?php _e( 'Ask Question', 'tk-loop-designer' ); ?></a></p>
			</div>

			<div style="float: left; overflow: auto; padding: 0 20px 0 20px; border-right: 1px solid #ddd;">
		        <h3><?php _e( 'Contribute your ideas.', 'tk-loop-designer' ); ?></h3>
		        <p><?php _e( 'Add ideas and vote in our', 'tk-loop-designer' ); ?> <a class="button button-secondary" href="https://themekraft.zendesk.com/hc/communities/public/topics/200001412-Loop-Designer-Ideas" target="_new"><?php _e( 'Ideas Forums', 'tk-loop-designer' ); ?></a></p>
			</div>


		</div>
		<br>

        <form method="post" action="options.php">
            <?php wp_nonce_field( 'update-options' ); ?>
            <?php settings_fields( 'tk_loop_designer_options' ); ?>
            <?php do_settings_sections( 'tk_loop_designer_options' ); ?>

        </form>

    </div><?php

}

/**
 * Register the admin settings
 *
 * @author Sven Lehnert
 * @package TK Loop Designer
 * @since 1.0
 */

add_action( 'admin_init', 'tk_loop_designer_register_admin_settings' );

function tk_loop_designer_register_admin_settings() {

    register_setting( 'tk_loop_designer_options', 'tk_loop_designer_options' );

    // Settings fields and sections
    add_settings_section( 'section_general', '', 'tk_loop_designer_general', 'tk_loop_designer_options' );

	add_settings_field( 'activate_license', '<h3>' . __( '1. Activate', 'tk-loop-designer') . '</h3>', 'tk_loop_designer_activate_license_keys', 'tk_loop_designer_options' , 'section_general' );
	add_settings_field( 'customizer_disabled', '<h3>' . __( '2. Start Creating', 'tk-loop-designer') . '</h3>', 'tk_loop_designer_customizer', 'tk_loop_designer_options' , 'section_general' );
	add_settings_field( 'using_loops', '<h3>' . __( '3. Use Your Loops', 'tk-loop-designer') . '</h3>', 'tk_loop_designer_usage', 'tk_loop_designer_options' , 'section_general' );
	add_settings_field( 'managing_loops', '<h3>' . __( '4. Manage Your Loops', 'tk-loop-designer') . '</h3>', 'tk_loop_designer_manage', 'tk_loop_designer_options' , 'section_general' );

}

/**
 * Important notice on top of the screen
 *
 * @author Sven Lehnert
 * @package TK Loop Designer
 * @since 1.0
 */

function tk_loop_designer_general() {

	//echo '<div style="padding: 20px; background: #f4f4f4; border-radius: 6px;"><h2>Getting Started</h2>';
    //echo '<p style="font-size: 17px;">Just follow these easy steps to get the most out of the Loop Designer.</p></div><br><br>';

}


/**
 * 1. Activate License Keys
 *
 * @author Sven Lehnert
 * @package TK Loop Designer
 * @since 1.0
 */

function tk_loop_designer_activate_license_keys(){ ?>

	<h3><?php _e( 'Activate your license keys here:', 'tk-loop-designer' ); ?></h3>

	<p><a href="<?php echo get_admin_url(); ?>admin.php?page=tk_loop_designer_license_registration_dashboard"  class="button-primary"><?php _e( 'Activate', 'tk-loop-designer' ); ?></a></p>

	<?php


}



/**
 * 2. Start the Loop Designer - Jump into the Customizer or turn Customizer support on/off.
 *
 * @author Sven Lehnert
 * @package TK Loop Designer
 * @since 1.0
 */

function tk_loop_designer_customizer(){ ?>

	<h3><?php _e( 'Create and customize your loops.', 'tk-loop-designer' ); ?></h3>

	<p><?php _e( 'Customize existing loops or create your own. All in the WP Customizer.', 'tk-loop-designer' ); ?></p>

	<p><a href="<?php echo get_admin_url(); ?>customize.php"  class="button-primary"><?php _e( 'Go to Customizer', 'tk-loop-designer' ); ?></a></p>

	<br>

	<div class="tk_adminbox" style="padding: 20px; background: #f4f4f4; border-radius: 6px; margin: 20px 0;">
		<h3><?php _e( 'Turn off Customizer Support', 'tk-loop-designer' ); ?></h3>
		<p><?php _e( 'This will disable the Loop Designer in the Customizer, but your already created loops will still be available. <br>
		This option can be very useful if you finalise your loops and want to stop editing them.', 'tk-loop-designer' ); ?></p>
		<?php
		 $options = get_option( 'tk_loop_designer_options' );

		 $customizer_disabled = 0;
		 if(isset( $options['customizer_disabled']))
		 	 $customizer_disabled = $options['customizer_disabled'];


	    ?><b><?php _e( 'Turn off Customizer:', 'tk-loop-designer' ); ?> </b> <input id='checkbox' name='tk_loop_designer_options[customizer_disabled]' type='checkbox' value='1' <?php checked( $customizer_disabled, 1  ) ; ?> /><?php

		submit_button(); ?>

	</div>

<?php

}

/**
 * 3. Using your loops in the theme
 *
 * @author Sven Lehnert
 * @package TK Loop Designer
 * @since 1.0
 */

function tk_loop_designer_usage(){ ?>

	<h3><?php _e( 'Use your loops in your theme!', 'tk-loop-designer' ); ?></h3>

	<p><?php _e( 'There are 3 ways to use your loops:', 'tk-loop-designer' ); ?></p>

	<div class="tk_adminbox" style="padding: 20px; background: #f4f4f4; border-radius: 6px; margin: 20px 0;">
		<p><b><?php _e( '1. Via Widget', 'tk-loop-designer' ); ?></b></p>
		<p></p><?php _e( 'Use the "List Posts" widget to add your list of posts in any sidebar or widgetarea.', 'tk-loop-designer' ); ?>
		<p><a href="<?php echo get_admin_url(); ?>widgets.php"  class="button-primary"><?php _e( 'Go to Widgets', 'tk-loop-designer' ); ?></a></p>
	</div>

	<div class="tk_adminbox" style="padding: 20px; background: #f4f4f4; border-radius: 6px; margin: 20px 0;">
		<p><b><?php _e( '2. Via Theme Options (in certain themes only*)', 'tk-loop-designer' ); ?></b></p>
		<p><?php _e( 'You can simply choose your new loop templates in the theme options for changing the list post style on your blog pages.', 'tk-loop-designer' ); ?></p>
		<p><?php _e( 'Go to X2 OPTIONS -> BLOG -> ARCHIVE VIEW -> POST LISTING STYLE', 'tk-loop-designer' ); ?></p>
		<p><?php _e( '* Just available in the x2 theme and CC 2.0 theme for now.', 'tk-loop-designer' ); ?></p>
	</div>

	<div class="tk_adminbox" style="padding: 20px; background: #f4f4f4; border-radius: 6px; margin: 20px 0;">
		<p><b><?php _e( '3. Coding', 'tk-loop-designer' ); ?></b></p>
		<p><?php _e( '...or use the loops via inserting the code yourself, for example for the standard blog loop in any WordPress theme.', 'tk-loop-designer' ); ?> </p>

		<b><?php _e( 'Helpful tutorials how to use the Loop Designer:', 'tk-loop-designer' ); ?></b>
		<ul class="article-list">
			<li>

			  <a target="_blank" href="http://support.themekraft.com/hc/en-us/articles/200037931-Hook-into-loop-templates"><?php _e( 'Hook into loop templates', 'tk-loop-designer' ); ?></a>
			</li>
			<li>

			  <a target="_blank" href="http://support.themekraft.com/hc/en-us/articles/200039432-Hook-into-loop-templates-examples"><?php _e( 'Hook into loop templates examples', 'tk-loop-designer' ); ?></a>
			</li>
			<li>

			  <a target="_blank" href="http://support.themekraft.com/hc/en-us/articles/200039442-Hook-Reference"><?php _e( 'Hook Reference', 'tk-loop-designer' ); ?></a>
			</li>
			<li>

			  <a target="_blank" href="http://support.themekraft.com/hc/en-us/articles/200039452-Use-via-PHP"><?php _e( 'Use via PHP', 'tk-loop-designer' ); ?></a>
			</li>

		</ul>
	</div>


	<br><?php


}


/**
 * 4. Managing your loops
 *
 * @author Sven Lehnert
 * @package TK Loop Designer
 * @since 1.0
 */

function tk_loop_designer_manage(){ ?>

	<h3><?php _e( 'Import, export or delete existing loops.', 'tk-loop-designer' ); ?></h3>

	<p><a href="<?php echo get_admin_url(); ?>admin.php?page=tk_loop_designer_import-export"  class="button-primary"><?php _e( 'Loop Manager', 'tk-loop-designer' ); ?></a></p>


	<br><?php


}

?>