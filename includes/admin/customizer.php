<?php

/**
 * WordPress Customizer initialization
 *
 * @author Sven Lehnert
 * @package TK Google Fonts
 * @since 1.0
 */
function tk_loop_designer_customizer_init(){
	add_action( 'customize_register', 'tk_loop_designer_customize_register' );
}
add_action( 'init', 'tk_loop_designer_customizer_init' );

/**
 * Registering for the WordPress Customizer
 *
 * @param WP_Customize_Manager $wp_customize
 *
 * @author Sven Lehnert
 * @package TK Google Fonts
 * @since 1.0
 */
function tk_loop_designer_customize_register( $wp_customize ) {

	// http://wordpress.stackexchange.com/questions/90188/adding-a-description-to-theme-customizer-controls
	class TK_Customize_Control extends WP_Customize_Control {
		public $description = ''; // Add this for the extra description.
		public function render_content() {
		?>
		<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<span><?php echo esc_html( $this->description ); ?></span>
			<input type="text" value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />
		</label>
		<?php
		}
	}

	// get the tk google fonts
	$tk_google_fonts_options = get_option('tk_google_fonts_options');

	// font family
	$tk_loop_designer_fonts  = Array (
		'arial, sans-serif'									=> 'Arial',
		'arial black, arial, sans-serif'					=> 'Arial Black',
		'helvetica, arial, sans-serif'						=> 'Helvetica',
		'century gothic, avant garde, arial, sans-serif'	=> 'Century Gothic',
		'impact, arial, sans-serif'							=> 'Impact',
		'times new roman, times'							=> 'Times New Roman',
		'garamond, times new roman, times, serif'			=> 'Garamond',
		'georgia, times, serif'								=> 'Georgia',
	);

	if(defined('TK_GOOGLE_FONTS')){
		if(isset($tk_google_fonts_options['selected_fonts'])){
			foreach ($tk_google_fonts_options['selected_fonts'] as $key => $tk_selected_font) {
				$tk_google_font_string = str_replace("+", " ", $tk_selected_font);
				$tk_loop_designer_fonts[$tk_google_font_string] = $tk_google_font_string;
			}
		}
	}
	// Boolean
	$boolean = array(
		'on'  => __( 'Enabled', 'tk-loop-designer' ),
		'off' => __( 'Disabled', 'tk-loop-designer' ),
	);

	$tk_loop_designer_options = get_option('tk_loop_designer_options');
	$element_i = 50;
	 if(isset( $tk_loop_designer_options['customizer_disabled']))
		return;


	$wp_customize->add_panel( 'tk_loop_designer_settings_panel', array(
		'priority'       => 10,
		'capability'     => 'edit_theme_options',
		'theme_supports' => '',
		'title'          => 'TK Loop Designer',
	) );

	$wp_customize->add_section( 'tk_loop_designer_settings', array(
		'title'          => __( 'GENERAL SETTINGS (create/manage)', 'tk-loop-designer' ),
		'priority'       => 900,
		'panel'  => 'tk_loop_designer_settings_panel',
	) );

	$wp_customize->add_setting( 'add_template_option', array(
	'default'        	=> __( 'Some default text for the textarea', 'tk-loop-designer' ),
	) );
	$wp_customize->add_control( new Loop_Designer_Add_Template_Option_Control( $wp_customize, 'add_template_option', array(
		'label'  		=> __( 'Textarea Setting', 'tk-loop-designer' ),
		'section'		=> 'tk_loop_designer_settings',
		'settings'		=> 'add_template_option',
		'priority'		=> $element_i
	) ) );
	$element_i ++;

	$tk_loop_designer_options = get_option('tk_loop_designer_options');

	if(isset($tk_loop_designer_options['templates'])) {

		foreach ($tk_loop_designer_options['templates'] as $key => $template) {

			$wp_customize->add_section( 'tk_loop_designer_settings'.$key, array(
				'title'          => __( '&#10153; '.$template, 'tk-loop-designer' ),
				'priority'       => 900,
				'panel'  => 'tk_loop_designer_settings_panel',
			) );


			$template_sanitize_title = $key;

			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_entry_label]', array(
				'type'		=> 'option',
				'transport'	=> 	'refresh',
			));
			$wp_customize->add_control( new TK_Loop_Designer_Customizer_Label($wp_customize, 'loop_template_entry_entry_label'.$template_sanitize_title, array(
				'settings'	=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_entry_label]',
				'label'		=> __( 'Entry', 'tk-loop-designer' ),
				'section'	=> 'tk_loop_designer_settings'.$key,
				'priority'	=> $element_i,
			)))	;
			$element_i ++;

			/**
			 * Post entry: clickable box or not?
			 */
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_clickable]', array(
				'type'		=> 'option',
				'transport'	=> 	'refresh',

			));
			$wp_customize->add_control( 'loop_template_entry_clickable'.$template_sanitize_title, array(
				'settings'	=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_clickable]',
				'label'		=> __( 'Clickable Box:', 'tk-loop-designer' ),
				'section'	=> 'tk_loop_designer_settings'.$key,
				'type'		=> 'select',
				'choices'	=> $boolean,
				'priority'	=> $element_i,

			));
			$element_i ++;

			//
			// post entry: List or Grid?
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_grid]', array(
				'default'	=> 'value2',
				'type'		=> 'option',
				'transport' => 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_entry_grid'.$template_sanitize_title, array(
				'settings'  => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_grid]',
				'label'  	=> __( 'Vertical list or enable Grid view?', 'tk-loop-designer' ),
				'section' 	=> 'tk_loop_designer_settings'.$key,
				'type'    	=> 'select',
				'choices'   => $boolean,
				'priority'	=> $element_i
			));
			$element_i ++;

			//
			//  post entry: background color or fade
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_background_color]', array(
				'default'           => '000',
				'sanitize_callback' => 'sanitize_hex_color',
				'capability'        => 'edit_theme_options',
				'type'          	=> 'option',
				'transport'   		=> 	'refresh',
				'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
				'sanitize_js_callback' 	=> 'maybe_hash_hex_color',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_background_color'.$template_sanitize_title, array(
				'settings'			=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_background_color]',
				'label'    			=> __( 'Background color', 'tk-loop-designer' ),
				'section' 			=> 'tk_loop_designer_settings'.$key,
				'priority'			=> $element_i
			)));
			$element_i ++;

			//
			//  post entry: background color fade top
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_background_color_top]', array(
				'default'           => '000',
				'sanitize_callback' => 'sanitize_hex_color',
				'capability'        => 'edit_theme_options',
				'type'          	=> 'option',
				'transport'   		=> 	'refresh',
				'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_background_color_top'.$template_sanitize_title, array(
				'settings'			=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_background_color_top]',
				'label' 			=> __( 'Background color (fade top)', 'tk-loop-designer' ),
				'section'			=> 'tk_loop_designer_settings'.$key,
				'priority'			=> $element_i
			)));
			$element_i ++;

			//
			//  post entry: Background hover effect
			//	Background color for the hover effect. <br>Recommended only when option clickable box is enabled.
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_background_color_hover]', array(
				'default'           => '000',
				'sanitize_callback' => 'sanitize_hex_color',
				'capability'        => 'edit_theme_options',
				'type'          	=> 'option',
				'transport'   		=> 	'refresh',
				'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_background_color_hover'.$template_sanitize_title, array(
				'settings'			=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_background_color_hover]',
				'label'				=> __( 'Background hover effect', 'tk-loop-designer' ),
				'section'			=> 'tk_loop_designer_settings'.$key,
				'priority'			=> $element_i
			)));
			$element_i ++;

			//
			//	Background image
			//	Upload a background image or choose from media library.
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_background_image]', array(
				'default'		=> false,
				'capability'	=> 'edit_theme_options',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'loop_template_background_image'.$template_sanitize_title, array(
				'label'			=> __( 'Background image', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_background_image]',
				'priority'		=> $element_i
			)));
			$element_i ++;


			// Image repeat array
			$img_repeat  = Array (
				'no-repeat'		=> 'no repeat',
				'repeat-x'		=> 'repeat horizontal',
				'repeat-y'		=> 'repeat vertical',
				'repeat'		=> 'repeat both',

			);

			//
			// Background image repeat
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_background_image_repeat]', array(
				'default'		=> 'value2',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_background_image_repeat'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_background_image_repeat]',
				'label'   		=> __( 'Background image repeat', 'tk-loop-designer' ),
				'section' 		=> 'tk_loop_designer_settings'.$key,
				'type'    		=> 'select',
				'choices'   	=> $img_repeat,
				'priority'		=> $element_i
			));
			$element_i ++;

			//
			//  post entry: width and height
			//	Template width, in pixel or %, write for example: "200px" or "50%"'
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_width]', array(
				'default'		=> '',
				'capability'	=> 'edit_theme_options',
				'type'			=> 'option',
				'transport'		=> 	'refresh',

			));
			$wp_customize->add_control( new TK_Customize_Control( $wp_customize, 'loop_template_width'.$template_sanitize_title, array(
				'label'       => __( 'Post entry width', 'tk-loop-designer' ),
				'description' => __( 'Template width, in pixel or %, write for example: "200px" or "50%"', 'tk-loop-designer' ),
				'section'     => 'tk_loop_designer_settings'.$key,
				'settings'    => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_width]',
				'priority'    => $element_i
			)));
			$element_i ++;

			//
			//  Post entry height
			//	Template height, in pixel, just enter a number, for example: "120"
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_height]', array(
				'default'		=> '',
				'capability'	=> 'edit_theme_options',
				'type'			=> 'option',
				'transport'		=> 	'refresh',

			));
			$wp_customize->add_control( 'loop_template_height'.$template_sanitize_title, array(
				'label'			=> __('Post entry height', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_height]',
				'priority'		=> $element_i
			));
			$element_i ++;

			//
			//  post entry: corner radius
			//	Post entries with rounded corners? <br>In px, just enter a number, for example "6"<br>** note: older IE doesn\'t support rounded corners..
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_corner_radius]', array(
				'default'		=> '',
				'capability'	=> 'edit_theme_options',
				'type'			=> 'option',
				'transport'		=> 	'refresh',

			));
			$wp_customize->add_control( new TK_Customize_Control( $wp_customize, 'loop_template_corner_radius'.$template_sanitize_title, array(
				'label'       => __( 'Corner radius', 'tk-loop-designer' ),
				'description' => __( 'In px, just enter a number, for example "6"', 'tk-loop-designer' ),
				'section'     => 'tk_loop_designer_settings'.$key,
				'settings'    => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_corner_radius]',
				'priority'    => $element_i
			)));
			$element_i ++;

			//
			//	post entry: border color
			//	Choose a border color for the post entry. ', 'loop_template_border_color
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_border_color]', array(
				'default'           => '000',
				'sanitize_callback' => 'sanitize_hex_color',
				'capability'        => 'edit_theme_options',
				'type'          	=> 'option',
				'transport'   		=> 	'refresh',
				'sanitize_callback' => 'sanitize_hex_color_no_hash',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_border_color'.$template_sanitize_title, array(
				'label'				=> __( 'Border color', 'tk-loop-designer' ),
				'section'			=> 'tk_loop_designer_settings'.$key,
				'settings'			=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_border_color]',
				'priority'			=> $element_i
			)));
			$element_i ++;

			//
			//	Box shadow color
			//	Choose a color for your box shadow.', 'loop_template_box_shadow_color
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_box_shadow_color]', array(
				'default'			=> '000',
				'sanitize_callback'	=> 'sanitize_hex_color',
				'capability'		=> 'edit_theme_options',
				'type'				=> 'option',
				'transport'			=> 	'refresh',
				'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
				'sanitize_js_callback' 	=> 'maybe_hash_hex_color',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_box_shadow_color'.$template_sanitize_title, array(
				'label'				=> __( 'Box shadow color', 'tk-loop-designer' ),
				'section'			=> 'tk_loop_designer_settings'.$key,
				'settings'			=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_box_shadow_color]',
				'priority'			=> $element_i
			)));
			$element_i ++;

			// Shadow Style
			$shadow_style = Array (
				'outside'		=> 'outside',
				'inside'		=> 'inside',
			);

			//
			// Box shadow style
			//	Box shadow style, inside or outside? <br>** note for inside effect: the shadowcolor should be brighter than the post-entry and background color.
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_box_shadow_style]', array(
				'default'		=> 'value2',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_box_shadow_style'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_box_shadow_style]',
				'label'			=> __( 'Box shadow style', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices'		=> $shadow_style,
				'priority'		=> $element_i
			));
			$element_i ++;

			// Link target
			$link_target  = Array (
				'_self'			=> __( 'Self', 'tk-loop-designer' ),
				'_blank'		=> __( 'Blank', 'tk-loop-designer' ),
			);

			//
			//	Show read more link?
			//	Show the additional \'read more\' link or not?
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_read_more_link]', array(
				'default'		=> 'value2',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_read_more_link'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_read_more_link]',
				'label'			=> __( 'Show read more link?', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices'		=> $boolean,
				'priority'		=> $element_i
			));
			$element_i ++;

			/**
			 * NEXT TAB
			 */

			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_image_label]', array(
				'type'		=> 'option',
				'transport'	=> 	'refresh',
			));
			$wp_customize->add_control( new TK_Loop_Designer_Customizer_Label($wp_customize, 'loop_template_entry_image_label'.$template_sanitize_title, array(
				'settings'	=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_image_label]',
				'label'		=> __( 'Featured Image', 'tk-loop-designer' ),
				'section'	=> 'tk_loop_designer_settings'.$key,
				'priority'	=> $element_i,
			)))	;
			$element_i ++;


			//
			//	Featured Image?
			//	Show the featured image?
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_show]', array(
				'default'		=> 'show',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_image_show'.$template_sanitize_title, array(
				'settings'	=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_show]',
				'label'		=> __( 'Featured Image?', 'tk-loop-designer' ),
				'section'	=> 'tk_loop_designer_settings'.$key,
				'type'		=> 'select',
				'choices'	=> $boolean,
				'priority'	=> $element_i
			));
			$element_i ++;

			//
			//	Image position?
			//	Featured image position
			//
			$img_pos  = Array (
				'posts-img-left-content-right'		=> __( 'left from title and content', 'tk-loop-designer' ),
				'posts-img-right-content-left'		=> __( 'right from title and content', 'tk-loop-designer' ),
				'posts-img-over-content'			=> __( 'over title and content', 'tk-loop-designer' ),
				'posts-img-between-title-content'	=> __( 'between title and content', 'tk-loop-designer' ),
				'posts-img-under-content'			=> __( 'under title and content', 'tk-loop-designer' ),
			);

			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_position]', array(
				'default'		=> 'show',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_image_position'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_position]',
				'label'			=> __( 'Image position?', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices'		=> $img_pos,
				'priority'		=> $element_i
			));
			$element_i ++;

			//
			//  Image width
			//	The width in pixel, just enter a number. For example: "150
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_width]', array(
				'default'		=> '',
				'capability'	=> 'edit_theme_options',
				'type'			=> 'option',
				'transport'		=> 	'refresh',

			));
			$wp_customize->add_control( new TK_Customize_Control( $wp_customize, 'loop_template_height'.$template_sanitize_title, array(
				'label'       => __( 'Image width', 'tk-loop-designer' ),
				'description' => __( 'The width in pixel, just enter a number. For example: "150"', 'tk-loop-designer' ),
				'section'     => 'tk_loop_designer_settings'.$key,
				'settings'    => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_width]',
				'priority'    => $element_i
			)));
			$element_i ++;

			//
			//  Image height
			//	The height in pixel, just enter a number. For example: "150"
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_height]', array(
				'default'		=> '',
				'capability'	=> 'edit_theme_options',
				'type'			=> 'option',
				'transport'		=> 	'refresh',

			));
			$wp_customize->add_control( new TK_Customize_Control( $wp_customize, 'loop_template_image_height'.$template_sanitize_title, array(
				'label'       => __( 'Image height', 'tk-loop-designer' ),
				'description' => __( 'The height in pixel, just enter a number. For example: "150"', 'tk-loop-designer' ),
				'section'     => 'tk_loop_designer_settings'.$key,
				'settings'    => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_height]',
				'priority'    => $element_i
			)));
			$element_i ++;

			//
			//  Image corner radius
			//	Images with rounded corners? In pixel, just enter a number, for example: "11"'
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_corner_radius]', array(
				'default'		=> '',
				'capability'	=> 'edit_theme_options',
				'type'			=> 'option',
				'transport'		=> 	'refresh',

			));
			$wp_customize->add_control( new TK_Customize_Control( $wp_customize, 'loop_template_entry_corner_radius'.$template_sanitize_title, array(
				'label'       => __( 'Corner radius', 'tk-loop-designer' ),
				'description' => __( 'In pixel, just enter a number, for example: "11"', 'tk-loop-designer' ),
				'section'     => 'tk_loop_designer_settings'.$key,
				'settings'    => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_corner_radius]',
				'priority'    => $element_i
			)));
			$element_i ++;

			//
			//	featured image: border color
			//	Choose a border color for the images.
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_border_color]', array(
				'default'			=> '000',
				'sanitize_callback'	=> 'sanitize_hex_color',
				'capability'		=> 'edit_theme_options',
				'type'				=> 'option',
				'transport'			=> 	'refresh',
				'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
				'sanitize_js_callback' 	=> 'maybe_hash_hex_color',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_image_border_color'.$template_sanitize_title, array(
				'label'		=> __( 'Border color', 'tk-loop-designer' ),
				'section'	=> 'tk_loop_designer_settings'.$key,
				'settings'	=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_border_color]',
				'priority'	=> $element_i
			)));
			$element_i ++;

			//
			//	featured image: box shadows
			//	Choose a shadow color for the images.
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_box_shadow_color]', array(
				'default'			=> '000',
				'sanitize_callback'	=> 'sanitize_hex_color',
				'capability'		=> 'edit_theme_options',
				'type'				=> 'option',
				'transport'			=> 	'refresh',
				'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
				'sanitize_js_callback' 	=> 'maybe_hash_hex_color',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_image_box_shadow_color'.$template_sanitize_title, array(
				'label'		=> __( 'Box shadow color', 'tk-loop-designer' ),
				'section'	=> 'tk_loop_designer_settings'.$key,
				'settings'	=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_image_box_shadow_color]',
				'priority'	=> $element_i
			)));
			$element_i ++;

			//
			// Box shadow style
			//	Box shadow style, inside or outside? <br>** note for inside effect:
			//	the shadowcolor should be brighter than the post-entry and background color.Should the shadow look like inside or outside?
			//	You need to chose a shadow color above first. <br>** note for inside effect:
			// the shadowcolor should be brighter than the post-entry and background color.
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_box_shadow_style]', array(
				'default'		=> 'value2',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_box_shadow_style'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_box_shadow_style]',
				'label'			=> __( 'Box shadow style', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices'		=> $shadow_style,
				'priority'		=> $element_i
			));
			$element_i ++;


			//
			// NEXT TAB Title
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_title_label]', array(
				'type'		=> 'option',
				'transport'	=> 	'refresh',
			));
			$wp_customize->add_control( new TK_Loop_Designer_Customizer_Label($wp_customize, 'loop_template_entry_title_label'.$template_sanitize_title, array(
				'settings'	=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_title_label]',
				'label'		=> __( 'Title', 'tk-loop-designer' ),
				'section'	=> 'tk_loop_designer_settings'.$key,
				'priority'	=> $element_i,
			)))	;
			$element_i ++;

			//
			//	The title
			//	Show the title?
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_hide]', array(
				'default'		=> 'show',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_title_hide'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_hide]',
				'label'			=> __( 'Show the title?', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices'		=> $boolean,
				'priority'		=> $element_i
			));
			$element_i ++;

			//
			//  Title font size
			//	Images with rounded corners? In pixel, just enter a number, for example: "11"'
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_size]', array(
				'default'		=> '',
				'capability'	=> 'edit_theme_options',
				'type'			=> 'option',
				'transport'		=> 	'refresh',

			));
			$wp_customize->add_control( new TK_Customize_Control( $wp_customize, 'loop_template_title_size'.$template_sanitize_title, array(
				'label'       => __( 'Title font size', 'tk-loop-designer' ),
				'description' => __( 'In pixel, just enter a number, for example: "30"', 'tk-loop-designer' ),
				'section'     => 'tk_loop_designer_settings'.$key,
				'settings'    => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_size]',
				'priority'    => $element_i
			)));
			$element_i ++;


			// $font_weight
			$font_weight = Array (
				'normal'	=> 'normal',
				'bold'		=> 'bold',
			);

			//
			//	font family
			//	Title font family - bold or normal?
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_font_weight]', array(
				'default'		=> 'show',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_title_font_weight'.$template_sanitize_title, array(
				'settings'	=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_font_weight]',
				'label'		=> __( 'Title font weight', 'tk-loop-designer' ),
				'section'	=> 'tk_loop_designer_settings'.$key,
				'type'		=> 'select',
				'choices'	=> $font_weight,
				'priority'	=> $element_i
			));
			$element_i ++;

			// $font_italic
			$font_italic  = Array (
				'normal'	=> 'normal',
				'italic'	=> 'italic',
			);

			//
			//	font style
			//	Title font style - italic or normal?
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_font_style]', array(
				'default'       => 'show',
				'type'          => 'option',
				'transport'   	=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_title_font_style'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_font_style]',
				'label'			=> __( 'Title font style', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices'		=> $font_italic,
				'priority'		=> $element_i
			));
			$element_i ++;

			//
			//  title: font color
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_color]', array(
				'default'           => '000',
				'sanitize_callback' => 'sanitize_hex_color',
				'capability'        => 'edit_theme_options',
				'type'              => 'option',
				'transport'         =>  'refresh',
				'sanitize_callback'     => 'sanitize_hex_color_no_hash',
				'sanitize_js_callback'  => 'maybe_hash_hex_color',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_title_color'.$template_sanitize_title, array(
				'label'             => __( 'Title font color', 'tk-loop-designer' ),
				'section'           => 'tk_loop_designer_settings'.$key,
				'settings'          => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_color]',
				'priority'          => $element_i
			)));
			$element_i ++;

			//
			//	Title text shadows
			//	The title text shadow color
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_text_shadow_color]', array(
				'default'           => '000',
				'sanitize_callback' => 'sanitize_hex_color',
				'capability'        => 'edit_theme_options',
				'type'          	=> 'option',
				'transport'		  	=> 	'refresh',
				'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
				'sanitize_js_callback' 	=> 'maybe_hash_hex_color',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_title_text_shadow_color'.$template_sanitize_title, array(
				'label'				=> __( 'Title text shadow color', 'tk-loop-designer' ),
				'section'			=> 'tk_loop_designer_settings'.$key,
				'settings'			=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_text_shadow_color]',
				'priority'			=> $element_i
			)));
			$element_i ++;


			// Box shadow style
			//	Shadow style <br>Should the shadow look like inside or outside?
			//	* You need to chose a shadow color above first.
			//	** note for inside effect: the shadowcolor should be brighter than the post-entry and background color.
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_text_shadow_style]', array(
				'default'		=> 'value2',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_title_text_shadow_style'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_title_text_shadow_style]',
				'label'			=> __( 'Title text shadow style', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices'		=> $shadow_style,
				'priority'		=> $element_i
			));
			$element_i ++;

			/**
			 * NEXT TAB Meta
			 */
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_meta_label]', array(
				'type'		=> 'option',
				'transport'	=> 	'refresh',
			));
			$wp_customize->add_control( new TK_Loop_Designer_Customizer_Label($wp_customize, 'loop_template_entry_meta_label'.$template_sanitize_title, array(
				'settings'	=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_meta_label]',
				'label'		=> __( 'Meta Data', 'tk-loop-designer' ),
				'section'	=> 'tk_loop_designer_settings'.$key,
				'priority'	=> $element_i,
			)))	;
			$element_i ++;

			//
			//  The category
			//  on or off. * checked = off = default *
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_category_show]', array(
				'default'   => 'off',
				'type'      => 'option',
				'transport' => 'refresh',
			));
			$wp_customize->add_control( 'loop_template_category_show' . $template_sanitize_title, array(
				'settings' => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_category_show]',
				'label'    => __( 'Show the category?', 'tk-loop-designer' ),
				'section'  => 'tk_loop_designer_settings'.$key,
				'type'     => 'select',
				'choices'  => $boolean,
				'priority' => $element_i,
			));
			$element_i ++;

			//
			//  The category link
			//  on or off. * checked = on = default *
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_make_category_link]', array(
				'default'   => 'on',
				'type'      => 'option',
				'transport' => 'refresh',
			));
			$wp_customize->add_control( 'loop_template_make_category_link' . $template_sanitize_title, array(
				'settings' => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_make_category_link]',
				'label'    => __( 'Make categories as links to appropriate archive pages?', 'tk-loop-designer' ),
				'section'  => 'tk_loop_designer_settings'.$key,
				'type'     => 'select',
				'choices'  => $boolean,
				'priority' => $element_i,
			));
			$element_i ++;

			//
			//  The date
			//  on or off. * checked = on = default *
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_date_show]', array(
				'default'   => 'off',
				'type'      => 'option',
				'transport' => 'refresh',
			));
			$wp_customize->add_control( 'loop_template_date_show' . $template_sanitize_title, array(
				'settings' => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_date_show]',
				'label'    => __( 'Show the date?', 'tk-loop-designer' ),
				'section'  => 'tk_loop_designer_settings'.$key,
				'type'     => 'select',
				'choices'  => $boolean,
				'priority' => $element_i,
			));
			$element_i ++;

			//
			//  The author
			//  on or off. * checked = off = default *
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_author_show]', array(
				'default'   => 'off',
				'type'      => 'option',
				'transport' => 'refresh',
			));
			$wp_customize->add_control( 'loop_template_author_show' . $template_sanitize_title, array(
				'settings' => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_author_show]',
				'label'    => __( 'Show the author?', 'tk-loop-designer' ),
				'section'  => 'tk_loop_designer_settings'.$key,
				'type'     => 'select',
				'choices'  => $boolean,
				'priority' => $element_i,
			));
			$element_i ++;

			//
			//  The author link
			//  on or off. * checked = on = default *
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_make_author_link]', array(
				'default'   => 'on',
				'type'      => 'option',
				'transport' => 'refresh',
			));
			$wp_customize->add_control( 'loop_template_make_author_link' . $template_sanitize_title, array(
				'settings' => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_make_author_link]',
				'label'    => __( 'Make authors as links to appropriate archive pages?', 'tk-loop-designer' ),
				'section'  => 'tk_loop_designer_settings'.$key,
				'type'     => 'select',
				'choices'  => $boolean,
				'priority' => $element_i,
			));
			$element_i ++;

			//
			//	Meta position?
			//	Choose meta position
			//
			$meta_pos = array(
				'posts-meta-above-title'   => __( 'above title', 'tk-loop-designer' ),
				'posts-meta-above-image'   => __( 'above image', 'tk-loop-designer' ),
				'posts-meta-above-content' => __( 'above content', 'tk-loop-designer' ),
				'posts-meta-under-content' => __( 'under content', 'tk-loop-designer' ),
			);

			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_meta_position]', array(
				'default'   => 'posts-meta-above-content',
				'type'      => 'option',
				'transport' => 'refresh',
			));
			$wp_customize->add_control( 'loop_template_meta_position' . $template_sanitize_title, array(
				'settings' => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_meta_position]',
				'label'    => __( 'Meta position?', 'tk-loop-designer' ),
				'section'  => 'tk_loop_designer_settings'.$key,
				'type'     => 'select',
				'choices'  => $meta_pos,
				'priority' => $element_i,
			));
			$element_i ++;


			/**
			 * NEXT TAB Content
			 */
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_content_label]', array(
				'type'		=> 'option',
				'transport'	=> 	'refresh',
			));
			$wp_customize->add_control( new TK_Loop_Designer_Customizer_Label($wp_customize, 'loop_template_entry_content_label'.$template_sanitize_title, array(
				'settings'	=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_entry_content_label]',
				'label'		=> __( 'Content', 'tk-loop-designer' ),
				'section'	=> 'tk_loop_designer_settings'.$key,
				'priority'	=> $element_i,
			)))	;
			$element_i ++;

			//
			//	The title
			//	show or hide. * checked = show = default *
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_show]', array(
				'default'		=> 'show',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_content_show'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_show]',
				'label'			=> __( 'Show the content?', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices' 		=> $boolean,
				'priority'		=> $element_i
			));
			$element_i ++;

			//
			//  content: height
			//	Content height, in px, just write the number
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_height]', array(
				'default'		=> '',
				'capability'	=> 'edit_theme_options',
				'type'			=> 'option',
				'transport'		=> 	'refresh',

			));
			$wp_customize->add_control( new TK_Customize_Control( $wp_customize, 'loop_template_content_height'.$template_sanitize_title, array(
				'label'       => __( 'Content height (in px, just enter number)', 'tk-loop-designer' ),
				'description' => __( 'in px, just write the number', 'tk-loop-designer' ),
				'section'     => 'tk_loop_designer_settings'.$key,
				'settings'    => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_height]',
				'priority'    => $element_i
			)));
			$element_i ++;

			//
			//  content: corner radius
			//  Content font size
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_font_size]', array(
				'default'       => '',
				'capability'    => 'edit_theme_options',
				'type'          => 'option',
				'transport'     =>  'refresh',

			));
			$wp_customize->add_control( 'loop_template_content_font_size'.$template_sanitize_title, array(
				'label'         => __( 'Content font size', 'tk-loop-designer' ),
				'section'       => 'tk_loop_designer_settings'.$key,
				'settings'      => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_font_size]',
				'priority'      => $element_i
			));
			$element_i ++;

			//
			//	font family
			//	Content font family
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_font_family]', array(
				'default'		=> 'show',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_content_font_family'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_font_family]',
				'label'			=> __( 'Content font family', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices'		=> $tk_loop_designer_fonts,
				'priority'		=> $element_i
			));
			$element_i ++;

			//
			//	content: font weight
			//	Title font family - bold or normal?
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_font_weight]', array(
				'default'		=> 'show',
				'type'			=> 'option',
				'transport'		=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_content_font_weight'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_font_weight]',
				'label'			=> __( 'Content font weight', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices'		=> $font_weight,
				'priority'		=> $element_i
			));
			$element_i ++;

			//
			//	Content font style
			//	Content font style: italic or normal?
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_font_style]', array(
				'default'       => 'show',
				'type'          => 'option',
				'transport'   	=> 	'refresh',
			));
			$wp_customize->add_control( 'loop_template_content_font_style'.$template_sanitize_title, array(
				'settings'		=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_font_style]',
				'label'			=> __( 'Content font style', 'tk-loop-designer' ),
				'section'		=> 'tk_loop_designer_settings'.$key,
				'type'			=> 'select',
				'choices'		=> $font_italic,
				'priority'		=> $element_i
			));
			$element_i ++;

			//
			//  content: font color
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_font_color]', array(
				'default'           => '000',
				'sanitize_callback' => 'sanitize_hex_color',
				'capability'        => 'edit_theme_options',
				'type'              => 'option',
				'transport'         =>  'refresh',
				'sanitize_callback'     => 'sanitize_hex_color_no_hash',
				'sanitize_js_callback'  => 'maybe_hash_hex_color',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_content_font_color'.$template_sanitize_title, array(
				'label'             => __( 'Content font color', 'tk-loop-designer' ),
				'section'           => 'tk_loop_designer_settings'.$key,
				'settings'          => 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_font_color]',
				'priority'          => $element_i
			)));
			$element_i ++;

			//
			//	content: text shadows
			//	The content text shadow
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_text_shadow_color]', array(
				'default'           => '000',
				'sanitize_callback' => 'sanitize_hex_color',
				'capability'        => 'edit_theme_options',
				'type'          	=> 'option',
				'transport'		  	=> 	'refresh',
				'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
				'sanitize_js_callback' 	=> 'maybe_hash_hex_color',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_content_text_shadow_color'.$template_sanitize_title, array(
				'label'				=> __( 'Content text shadow color', 'tk-loop-designer' ),
				'section'			=> 'tk_loop_designer_settings'.$key,
				'settings'			=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_text_shadow_color]',
				'priority'			=> $element_i
			)));
			$element_i ++;

			//
			//	content: link color
			//	The content text shadow
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_link_color]', array(
				'default'           => '000',
				'sanitize_callback' => 'sanitize_hex_color',
				'capability'        => 'edit_theme_options',
				'type'          	=> 'option',
				'transport'		  	=> 	'refresh',
				'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
				'sanitize_js_callback' 	=> 'maybe_hash_hex_color',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_content_link_color'.$template_sanitize_title, array(
				'label'				=> __( 'Content link color', 'tk-loop-designer' ),
				'section'			=> 'tk_loop_designer_settings'.$key,
				'settings'			=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_link_color]',
				'priority'			=> $element_i
			)));
			$element_i ++;

			//
			//	content: link color hover
			//
			$wp_customize->add_setting('tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_link_color_hover]', array(
				'default'           => '000',
				'sanitize_callback' => 'sanitize_hex_color',
				'capability'        => 'edit_theme_options',
				'type'          	=> 'option',
				'transport'   		=> 	'refresh',
				'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
				'sanitize_js_callback' 	=> 'maybe_hash_hex_color',

			));
			$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'loop_template_content_link_color_hover'.$template_sanitize_title, array(
				'label'				=> __( 'Content link color hover', 'tk-loop-designer' ),
				'section'			=> 'tk_loop_designer_settings'.$key,
				'settings'			=> 'tk_loop_designer_options[template_options][' . $template_sanitize_title . '][loop_template_content_link_color_hover]',
				'priority'			=> $element_i
			)));
			$element_i ++;

		}
	}

}

/**
 * Here comes the resulting CSS output for the frontend!
 *
 * @author Sven Lehnert
 * @package TK Google Fonts
 * @since 1.0
 */
function tk_loop_designer_customizer_css(){

	$tk_loop_designer_options = get_option('tk_loop_designer_options');

	if(isset( $tk_loop_designer_options['customizer_disabled']))
		return;

	?><style type="text/css"><?php

		if(  get_theme_mod('h1_font', '') != 'none' )
			echo 'h1, h1 a, h1 a:hover { font-family:'. get_theme_mod('h1_font') . '; } ';

	?></style>

<?php
}
add_action( 'wp_head', 'tk_loop_designer_customizer_css', 99999 );

/**
 * WordPress Customizer Preview init
 *
 * @author Sven Lehnert
 * @package TK Google Fonts
 * @since 1.0
 */
function tk_loop_designer_customize_preview_init(){
	$tk_loop_designer_options = get_option('tk_loop_designer_options');

	if(isset( $tk_loop_designer_options['customizer_disabled']))
		return;

	wp_enqueue_script(
		'loop_designer_customize_js',
		plugins_url('/js/customizer.js', __FILE__),
		array( 'jquery','customize-preview' ),
		'',
		true
	);
}
add_action( 'customize_preview_init', 'tk_loop_designer_customize_preview_init');



function TK_Loop_Designer_Customizer_Label($wp_customize) {

	class TK_Loop_Designer_Customizer_Label extends WP_Customize_Control {

		public function render_content()
		{
			?>
			<label>
			<h2><?php echo $this->label ?></h2>
			</label>

		<?php
		}
	}
}
add_action( 'customize_register', 'TK_Loop_Designer_Customizer_Label' );


/**
 * @param WP_Customize_Manager $wp_customize
 */
function tk_loop_designer_add_template_option($wp_customize) {
	class Loop_Designer_Add_Template_Option_Control extends WP_Customize_Control {

		public function render_content() {
			?>

			<script>

			jQuery(window).load(function(){

				jQuery( "#tk_loop_designer_add_template_button" ).click(function() {
					var action = jQuery(this);
					var template_from = jQuery('#bf_create_template_from').val()
					var template_name = jQuery('#bf_create_template_name').val()

					if(template_name == ''){
						jQuery( '#bf_create_template_name' ).css('border-color', '#3399FF');
						jQuery( '#bf_create_template_name').attr('placeholder', 'Add a Name first');
						return false;
					}

					jQuery.ajax({
						type: 'POST',
						url: ajaxurl,
						data: {"action": "tk_loop_designer_add_template", "template_name": template_name, "template_from": template_from},
						success: function(data){
							location.reload();
						},
						error: function() {
							alert('Something went wrong.. ;-(sorry)');
						}
					});

				});

			});
			</script>

			<h2>Create new</h2>
			<?php
			$tk_loop_designer_options = get_option('tk_loop_designer_options');


			if(isset($tk_loop_designer_options['templates'])) {
				?>

				<label>
				<span class="customize-control-title"><?php _e( 'Select existing template as boilerplate', 'tk-loop-designer' ); ?></span>
				<select id="bf_create_template_from"><?php
					foreach ($tk_loop_designer_options['templates'] as $key => $template) {
						echo '<option value="'.$key.'">'. $template . '</option>';
					}
				?></select></label><?php
			}
			?>
			<br>
			<label>
				<span class="customize-control-title"><?php _e( 'Please enter a template name', 'tk-loop-designer' ); ?></span>
				<input placeholder="New Template Name" type="text" id="bf_create_template_name">
			</label>
			<br><br>
			<a href="#" class="button-secondary" id="tk_loop_designer_add_template_button" ><?php _e( 'Create', 'tk-loop-designer' ); ?></a>
			<br><br>
			<h2>Manage</h2>

			<label>
				<span class="customize-control-title"><?php _e( 'Manage template!', 'tk-loop-designer' ); ?></span>
				<a href="admin.php?page=tk_loop_designer_import-export" target="_blank" class="button-secondary" id="tk_loop_designer_settings_page" ><?php _e( 'Manage Loop Templates', 'tk-loop-designer' ); ?></a>
			</label>


		   <?php
		}


	}

}
add_action( 'customize_register', 'tk_loop_designer_add_template_option' );

/**
 * Ajax call back function to add a form element
 *
 * @author Sven Lehnert
 * @package TK Google Fonts
 * @since 1.0
 */
function tk_loop_designer_add_template(){

	if(isset($_POST['template_name']))
		$template_name = $_POST['template_name'];

	if(empty($template_name))
		return;

	$template_slug = sanitize_title($template_name);

	if(isset($_POST['template_from']))
		$template_from = $_POST['template_from'];

	if(empty($template_from))
		return;


	$tk_loop_designer_options = get_option('tk_loop_designer_options');

	if(isset($tk_loop_designer_options['templates'][$template_slug])){
		$md5_tmp = substr(md5(time() * rand()), 0, 5);
		$template_slug = $template_slug . '-' . $md5_tmp;
	}

	$tk_loop_designer_options['templates'][$template_slug]			= $template_name;
	$tk_loop_designer_options['template_options'][$template_slug]	= $tk_loop_designer_options['template_options'][$template_from];

	update_option("tk_loop_designer_options", $tk_loop_designer_options);

	die();

}
add_action( 'wp_ajax_tk_loop_designer_add_template', 'tk_loop_designer_add_template' );
add_action( 'wp_ajax_nopriv_tk_loop_designer_add_template', 'tk_loop_designer_add_template' );

/**
 * Ajax call back function to add a form element
 *
 * @author Sven Lehnert
 * @package TK Google Fonts
 * @since 1.0
 */
function tk_loop_designer_load_template(){

	if(isset($_POST['template_name']))
		$template_name = $_POST['template_name'];

	if(empty($template_name))
		return;


	$tk_loop_designer_options = get_option('tk_loop_designer_options');

	echo '<li id="customize-control-tk_select_template" class="customize-control customize-control-select tk_ld_added">';
	echo '	<label>';
	echo '		<span class="customize-control-title">' . $tk_loop_designer_options['templates'][$template_name] . ':</span>';
	echo '	</label>';
	echo '</li>';

	die();


}
//add_action( 'wp_ajax_tk_loop_designer_load_template', 'tk_loop_designer_load_template' );
//add_action( 'wp_ajax_nopriv_tk_loop_designer_load_template', 'tk_loop_designer_load_template' );

/**
 * Ajax call back function to delete a form element
 *
 * @author Sven Lehnert
 * @package TK Google Fonts
 * @since 1.0
 */
function tk_loop_designer_delete_template(){

	if(isset($_POST['template_name']))
		$template_name = $_POST['template_name'];

	if(empty($template_name))
		return;

	$tk_loop_designer_options = get_option('tk_loop_designer_options');
	if(is_array($template_name)){
		foreach ($template_name as $template) {
			unset( $tk_loop_designer_options['templates'][$template] );
			unset( $tk_loop_designer_options['template_options'][$template] );
		}
	} else {
		unset( $tk_loop_designer_options['templates'][$template_name] );
		unset( $tk_loop_designer_options['template_options'][$template_name] );
	}

	$tk_loop_designer_options['select_template'] = array_shift(array_values($tk_loop_designer_options['templates']));

	update_option("tk_loop_designer_options", $tk_loop_designer_options);
	die();
}
add_action( 'wp_ajax_tk_loop_designer_delete_template', 'tk_loop_designer_delete_template' );
add_action( 'wp_ajax_nopriv_tk_loop_designer_delete_template', 'tk_loop_designer_delete_template' );

?>