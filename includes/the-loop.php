<?php
/**
 * List posts
 *
 * @param $atts
 * @param null $content
 * @return string
 */
function tk_loop_designer_list_posts($atts, $content = null) {
	global $post, $the_lp_query;

	$this_post = $post;

	extract(shortcode_atts(array(
		'featured_id'                 => '',
		'amount'                      => '12',
		'posts_per_page'              => '4',
		'category_name'               => '0',
		'page_id'                     => '',
		'post_type'                   => 'post',
		'orderby'                     => '',
		'order'                       => '',
		'show_sticky'                 => 'off',
		'img_position'                => 'mouse_over',
		'height'                      => 'auto',
		'show_pagination'             => 'show',
		'pagination_ajax_effect'      => 'fadeOut_fadeIn',
		'featured_posts_image_width'  => '222',
		'featured_posts_image_height' => '160',
	), $atts));

	if ( $featured_id == '') {
		$featured_id = substr(md5(rand()), 0, 10);
	}
	if ( $page_id != '') {
		$page_id = explode(',',$page_id);
	}
	if ( $height != 'auto' ) {
		$height = $height.'px';
	}
	if ( $img_position == 'posts-img-between-title-content' ) {
		$margintop = 'margin-top: 10px;';
	}

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	// Get IDs of sticky posts
	//$sticky_posts = get_option( 'sticky_posts' );

	// Get the sticky posts query
	$most_recent_sticky_post = array(
		//'post__in'            => $sticky_posts,
        'meta_key' => '_featured',
        'meta_value' => 'yes',
		'category_name'       => $category_name,
		'posts_per_page'      => $posts_per_page,
		'amount'              => $amount,
		'orderby'             => $orderby,
		'order'               => $order,
		'post_type'           => $post_type,
		'paged'               => $paged,
	);

	// Get the normal query
	$list_post_query_args = array(
		'amount'              => $amount,
		'posts_per_page'      => $posts_per_page,
		'orderby'             => $orderby,
		'order'               => $order,
		'post_type'           => $post_type,
		'post__in'            => $page_id,
		'category_name'       => $category_name,
		'paged'               => get_query_var('paged'),
		'ignore_sticky_posts' => 1,
	);

	$list_post_query_args = $show_sticky == 'on' ? $most_recent_sticky_post : $list_post_query_args;

//    echo '<pre>';
//    print_r($list_post_query_args);
//    echo '</pre>';

	remove_all_filters('posts_orderby');

	$list_post_query[ $featured_id ] = new WP_Query( $list_post_query_args );
	$the_lp_query = $list_post_query[ $featured_id ];

	$tk_loop_designer_template = tk_loop_designer_get_the_loop( $img_position, $featured_id, $show_pagination );

	wp_reset_query();
	$post = $this_post;

	return $tk_loop_designer_template;

}
add_shortcode('tk_list_posts', 'tk_loop_designer_list_posts');

// hooking the featured image :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
add_action( 'tk_loop_designer_before_loop_item', 'tk_loop_designer_list_posts_featured_image' );
function tk_loop_designer_list_posts_featured_image(){
	global $list_post_atts;

	if ( $list_post_atts ) {
		extract( $list_post_atts );
	}

	switch ( $img_position ) {
		case 'posts-img-left-content-right':
				add_action( 'tk_loop_designer_before_entry_inner', 'tk_loop_designer_list_posts_get_featured_image', 1 );
			break;
		case 'posts-img-right-content-left':
				add_action( 'tk_loop_designer_before_entry_inner', 'tk_loop_designer_list_posts_get_featured_image', 1 );
			break;
		case 'posts-img-over-content':
				add_action( 'tk_loop_designer_before_loop_title', 'tk_loop_designer_list_posts_get_featured_image', 10 );
			break;
		case 'posts-img-between-title-content':
				add_action( 'tk_loop_designer_after_loop_title', 'tk_loop_designer_list_posts_get_featured_image', 10 );
			break;
		case 'posts-img-under-content':
				add_action( 'tk_loop_designer_after_loop_title', 'tk_loop_designer_list_posts_get_featured_image', 30 );
			break;
		default:
				add_action( 'tk_loop_designer_before_entry_inner', 'tk_loop_designer_list_posts_get_featured_image', 1 );
			break;
	}
}

function tk_loop_designer_list_posts_get_featured_image(){
	global $list_post_atts, $post;

	// Extract the arguments
	if ( $list_post_atts ) {
		extract( $list_post_atts );
	}

	$featured_image = '';

	if ( $show_img == 'on' ) :

		$img_class = isset( $reflect ) ? $reflect : '';

		if ( $clickable == 'on') {
			$featured_image = '<span class="ld-img link">' . get_the_post_thumbnail( $post->ID, array( $featured_posts_image_width, $featured_posts_image_height ), "class={$img_class}" ) . '</span>';
		} else {
			$featured_image = '<a class="ld-img" href="' . get_permalink( $post->ID ) . '" title="' . get_the_title() . '" target="' . $link_target . '">' . get_the_post_thumbnail( $post->ID, array( $featured_posts_image_width, $featured_posts_image_height ), "class={$img_class}" ) . '</a>';
		}

	endif;

	do_action( 'tk_loop_designer_before_loop_image' );

	echo $featured_image;

	do_action( 'tk_loop_designer_after_loop_image' );
}

// hooking the meta :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
add_action( 'tk_loop_designer_before_loop_item', 'tk_loop_designer_list_posts_meta' );
function tk_loop_designer_list_posts_meta(){
	global $list_post_atts;

	if ( $list_post_atts ) {
		extract( $list_post_atts );
	}

	switch ( $meta_position ) {
		case 'posts-meta-above-title':
				add_action( 'tk_loop_designer_before_loop_title', 'tk_loop_designer_list_get_posts_meta' );
			break;
		case 'posts-meta-above-image':
				add_action( 'tk_loop_designer_before_loop_image', 'tk_loop_designer_list_get_posts_meta' );
			break;
		case 'posts-meta-above-content':
				add_action( 'tk_loop_designer_before_loop_content', 'tk_loop_designer_list_get_posts_meta' );
			break;
		case 'posts-meta-under-content':
				add_action( 'tk_loop_designer_after_loop_content', 'tk_loop_designer_list_get_posts_meta' );
			break;
		default:
				add_action( 'tk_loop_designer_before_loop_content', 'tk_loop_designer_list_get_posts_meta' );
			break;
	}
}

function tk_loop_designer_list_get_posts_meta() {
	global $list_post_atts, $post;

	// Extract the arguments.
	if ( $list_post_atts ) {
		extract( $list_post_atts );
	}
	
	echo '<div class="ld-entry-meta">';

		$cats_array = get_the_category();
		$cats = '';

		if ( $make_category_link == 'on' ) {
			$cats = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'tk-loop-designer' ) );
		} else {

			foreach ( (array)$cats_array as $key => $cat ) {
				$cats .= $cat->name . ', ';
			}

		}

		$cats = apply_filters( 'tk_loop_designer_list_posts_meta_cats', sprintf( '<span class="entry-cats">%s</span>', trim( $cats, ', ' ) ), $make_category_link );
		$date = apply_filters( 'tk_loop_designer_list_posts_meta_date', sprintf( '<span class="entry-date"><time class="entry-date" datetime="%1$s">%2$s</time></span>',
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		) );

		if ( $make_author_link == 'on' ) {
			$author = apply_filters( 'tk_loop_designer_list_posts_meta_author', sprintf( '<span class="entry-author"><a class="url fn n" href="%1$s" rel="author">%2$s</a></span>',
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				get_the_author() ),
			$make_author_link );
		} else {
			$author = apply_filters( 'tk_loop_designer_list_posts_meta_author', sprintf( '<span class="entry-author">%s</span>', get_the_author() ), $make_author_link );
		}

		$meta_template = apply_filters( 'tk_loop_designer_list_posts_meta_template', '%%CATS%% %%DATE%% %%AUTHOR%%' );

		// Check 'show_cats' option.
		if ( $show_category == 'on' ) {
			$meta_template = str_replace( '%%CATS%%', $cats, $meta_template );
		} else {
			$meta_template = str_replace( '%%CATS%%', '', $meta_template );
		}

		// Check 'show_date' option.
		if ( $show_date == 'on' ) {
			$meta_template = str_replace( '%%DATE%%', $date, $meta_template );
		} else {
			$meta_template = str_replace( '%%DATE%%', '', $meta_template );
		}

		// Check 'show_author' option.
		if ( $show_author == 'on' ) {
			$meta_template = str_replace( '%%AUTHOR%%', $author, $meta_template );
		} else {
			$meta_template = str_replace( '%%AUTHOR%%', '', $meta_template );
		}

		$meta = apply_filters( 'tk_loop_designer_list_posts_meta', $meta_template );
		echo $meta;

	echo '</div>';
}

// hooking the content :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
add_action( 'tk_loop_designer_after_loop_title', 'tk_loop_designer_list_posts_content', 20 );
function tk_loop_designer_list_posts_content(){
	global $list_post_atts, $post;

	// Extract the arguments
	if ( $list_post_atts ) {
		extract( $list_post_atts );
	}

	do_action( 'tk_loop_designer_before_loop_content' );

	// check if the excerpt should be displayed
	if ( $show_the_excerpt == 'on') {
		echo apply_filters('tk_loop_designer_the_excerpt','<p class="ld-post-excerpt">' . get_the_excerpt() . '</p>');
	}

	// check if the read more link should be displayed
	if ( $show_more_link == 'on' ) :

		if ( $clickable == 'on') {
			echo apply_filters('tk_loop_designer_read_more_link','<p class="ld-readmore"><span class="link">' . __('read more','tk-loop-designer') . '</span></p>');
		} else {
			echo apply_filters('tk_loop_designer_read_more_link','<p class="ld-readmore"><a href="' . get_permalink( $post->ID ) . '" target="<?php echo $link_target; ?>">' . __('read more','tk-loop-designer') . '</a></p>');
		}

	endif;

	do_action( 'tk_loop_designer_after_loop_content' );
}

/**
 * Add a div class clear if list style is chosen
 *
 * @package TK Loop Designer
 * @since 0.1 beta
 */
add_action( 'tk_loop_designer_after_loop_item', 'tk_loop_designer_add_div_clear', 9999);
function tk_loop_designer_add_div_clear(){
	global $list_post_atts;

	if($list_post_atts){
		extract($list_post_atts);
	}

	if( $list_post_template_entry_grid != true ) {
		echo '<div class="clear"></div>';
	}

}

/**
 * Add a CSS class "grid" to list post entry if grid style if chosen
 *
 * @package TK Loop Designer
 * @since 0.1 beta
 */
add_action( 'tk_loop_designer_listposts_class', 'tk_loop_designer_add_grid_class');
function tk_loop_designer_add_grid_class() {
	global $list_post_atts;

	if($list_post_atts)
		extract($list_post_atts);

	// IF IT'S A GRID: add a class to list post entry container via add_action!
	if( $list_post_template_entry_grid == true )
		echo ' grid ';

}

/**
 * Locate a template
 *
 * @package TK Loop Designer
 * @since 0.1 beta
 */
function tk_loop_designer_locate_template($file) {
	if (locate_template(array($file), false)) {
		locate_template(array($file), true);
	} else {
		include (LOOP_DESIGNER_TEMPLATE_PATH . $file);
	}
}

?>