<?php

function tk_loop_designer_the_loop( $template_slug, $tk_query_id, $show_pagination ){

	echo tk_loop_designer_get_the_loop( $template_slug, $tk_query_id, $show_pagination );

}

	function tk_loop_designer_get_the_loop( $template_slug, $tk_query_id, $show_pagination ){
		global $the_lp_query;
		$tk_loop_designer_template = '';
		tk_loop_designer_options($template_slug);

		ob_start();
			tk_loop_designer_locate_template( '/templates/the-loop.php' );
			$tk_loop_designer_template .= ob_get_contents();
		ob_end_clean();

		$tk_loop_designer_template .='<div class="clear"></div>';

		// Pagination starts
		if($show_pagination == 'show'){
			$tk_loop_designer_template .='<div id="' . $tk_query_id . '" class="navigation">';
			$tk_loop_designer_template .='<div class="alignleft">'. get_next_posts_link('&laquo; Older Entries', $the_lp_query->max_num_pages ) .'</div>';
			$tk_loop_designer_template .='<div class="alignright">' . get_previous_posts_link('Newer Entries &raquo;') .'</div>';
			$tk_loop_designer_template .='</div><!-- End navigation -->';
		}

		if($show_pagination == 'pagenavi' ){
			if(function_exists('wp_pagenavi')){
				ob_start();
					wp_pagenavi( array( 'query' => $the_lp_query) );
					$tk_loop_designer_template_wp_pagenavi = ob_get_clean();
					$tk_loop_designer_template_wp_pagenavi = str_replace('class=\'wp-pagenavi\'', 'id="'.$tk_query_id.'" class="wp-pagenavi navigation"', $tk_loop_designer_template_wp_pagenavi);

					$tk_loop_designer_template .= $tk_loop_designer_template_wp_pagenavi;
			}
		}
		wp_reset_postdata();

		return '<div id="featured_posts'.$tk_query_id.'"><div id="list_posts'.$tk_query_id.'" class="loop-designer list-posts-all">'.$tk_loop_designer_template.'</div></div>';
	}

function tk_loop_designer_load_default_templates(){

		$tk_loop_designer = get_option('tk_loop_designer_options');

		if ( !isset($tk_loop_designer['templates']) || count($tk_loop_designer['templates']) == 0 ){

			$loop_templates_directory = array_diff(scandir(LOOP_DESIGNER_LOOP_TEMPLATES_PATH), array('..', '.'));
			$tk_loop_designer_options = array(
				'templates'         => array(),
				'template_options'  => array()
			);

			foreach ($loop_templates_directory as $key => $template) {

				$tk_loop_designer_templates = unserialize( file_get_contents(LOOP_DESIGNER_LOOP_TEMPLATES_PATH . $template, true));

				foreach ($tk_loop_designer_templates['templates'] as $key => $template_options) {


					if(isset($tk_loop_designer_options['templates'][$key]) && isset($tk_loop_designer_templates['templates'][$key])){
						$md5_tmp = substr(md5(time() * rand()), 0, 5);
						$template_slug = 'Copy-of-'. $key . '-' . $md5_tmp;
						$template_name = 'Copy of '. $tk_loop_designer_templates['templates'][$key] . '-' . $md5_tmp;
					} else {
						$template_slug = $key;
						$template_name = $tk_loop_designer_templates['templates'][$key];
					}


					$tk_loop_designer_options['templates'][$template_slug]			= $template_name;
					$tk_loop_designer_options['template_options'][$template_slug]	= $tk_loop_designer_templates['template_options'][$key];

				}

			}
			update_option("tk_loop_designer_options", $tk_loop_designer_options);
		}
}

function tk_loop_designer_get_template_slug() {
	global $list_post_atts;
	return $list_post_atts['template_name'];
}

function tk_loop_designer_options($template_name){
	global $list_post_atts;

	$loop_designer_options = get_option('tk_loop_designer_options');
	$template_options      = $loop_designer_options['template_options'];

	//$template_name					= isset($template_options[$template_name]['loop_template_name']) ? sanitize_title($template_options[$template_name]['loop_template_name']) : '';
	$show_img                      = isset($template_options[$template_name]['loop_template_image_show']) ? $template_options[$template_name]['loop_template_image_show'] : '';
	$img_position                  = isset($template_options[$template_name]['loop_template_image_position']) ? $template_options[$template_name]['loop_template_image_position'] : '';
	$clickable                     = isset($template_options[$template_name]['loop_template_entry_clickable']) ? $template_options[$template_name]['loop_template_entry_clickable'] :'';
	$list_post_template_entry_grid = isset($template_options[$template_name]['loop_template_entry_grid']) ? $template_options[$template_name]['loop_template_entry_grid'] : '';
	$link_target                   = isset($template_options[$template_name]['loop_template_link_target']) ? $template_options[$template_name]['loop_template_link_target'] : '';
	$show_more_link                = isset($template_options[$template_name]['loop_template_read_more_link']) ? $template_options[$template_name]['loop_template_read_more_link'] : '';
	$show_the_excerpt              = isset($template_options[$template_name]['loop_template_content_show']) ? $template_options[$template_name]['loop_template_content_show'] : '';
	$content_height                = isset($template_options[$template_name]['loop_template_content_height']) ? $template_options[$template_name]['loop_template_content_height'] : '';
	$show_title                    = isset($template_options[$template_name]['loop_template_title_hide']) ? $template_options[$template_name]['loop_template_title_hide'] : '';
	$show_category                 = isset($template_options[$template_name]['loop_template_category_show']) ? $template_options[$template_name]['loop_template_category_show'] : '';
	$show_date                     = isset($template_options[$template_name]['loop_template_date_show']) ? $template_options[$template_name]['loop_template_date_show'] : '';
	$show_author                   = isset($template_options[$template_name]['loop_template_author_show']) ? $template_options[$template_name]['loop_template_author_show'] : '';
	$meta_position                 = isset($template_options[$template_name]['loop_template_meta_position']) ? $template_options[$template_name]['loop_template_meta_position'] : '';
	$make_category_link            = isset($template_options[$template_name]['loop_template_make_category_link']) ? $template_options[$template_name]['loop_template_make_category_link'] : '';
	$make_author_link            = isset($template_options[$template_name]['loop_template_make_author_link']) ? $template_options[$template_name]['loop_template_make_author_link'] : '';

	// sets the image width and height if there's a value.
	// if no value set, it becomes a 9999 = adapt the size automatically.
	if ( !empty($template_options[$template_name]['loop_template_image_height'])) {
		$featured_posts_image_height = $template_options[$template_name]['loop_template_image_height'];
	} else {
		$featured_posts_image_height = 9999;
	}

	if (!empty($template_options[$template_name]['loop_template_image_width']) ) {
		$featured_posts_image_width = $template_options[$template_name]['loop_template_image_width'];
	} else {
		$featured_posts_image_width = 9999;
	}

	$list_post_atts = array(
		'show_title'                    => $show_title,
		'show_img'                      => $show_img,
		'template_name'                 => $template_name,
		'img_position'                  => $img_position,
		// 'height'                        => $height,
		// 'featured_id'                   => $featured_id,
		'featured_posts_image_width'    => $featured_posts_image_width,
		'featured_posts_image_height'   => $featured_posts_image_height,
		// 'margintop'                     => $margintop,
		'link_target'                   => $link_target,
		'show_the_excerpt'              => $show_the_excerpt,
		'show_more_link'                => $show_more_link,
		'arrayindex'                    => $template_name,
		'clickable'                     => $clickable,
		'list_post_template_entry_grid' => $list_post_template_entry_grid,
		'content_height'                => $content_height,
		'show_category'                 => $show_category,
		'show_date'                     => $show_date,
		'show_author'                   => $show_author,
		'meta_position'                 => $meta_position,
		'make_category_link'            => $make_category_link,
		'make_author_link'              => $make_author_link,
	);

	// echo '<pre>';
	// print_r($list_post_atts);
	// echo '</pre>';
}

add_filter( 'wp_head', 'tk_loop_designer_css',9999);
function tk_loop_designer_css(){

	$tk_loop_designer_options	= get_option('tk_loop_designer_options');

	ob_start();

	if(!isset($tk_loop_designer_options['template_options']))
		return;

	foreach ($tk_loop_designer_options['template_options'] as $template_name => $template_option) :
	echo '<!-- TK Loop Designer - ' . $template_name . ' Template CSS //-->';
	?>

		<style>

			/* make the wrapper first 100% wide */
			div.list-posts-all.<?php echo $template_name; ?> {
				width: 100%;
			}

			/* post entry styles */
			.loop-designer div.listposts.<?php echo $template_name; ?> {
				<?php if ( !empty($template_option['loop_template_background_color']) || !empty($template_option['loop_template_border_color']) || !empty($template_option['loop_template_background_color_hover']) || !empty($template_option['loop_template_box_shadow_color']) ) { ?>
					/* Add some padding left and right when a color is chosen for background, hover, border or shadow */
						padding: 0 10px;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_background_color'])) { ?>
					/* BG color fallback */
						background: none repeat scroll 0 0 #<?php echo $template_option['loop_template_background_color'] ?>;
					<?php if ( !empty($template_option['loop_template_background_color_top']) ) { ?>
					/* Firefox: */
						background: -moz-linear-gradient(center top, #<?php echo $template_option['loop_template_background_color_top']; ?>, #<?php echo $template_option['loop_template_background_color']; ?>);
					/* Chrome, Safari:*/
						background: -webkit-gradient(linear, left top, left center, from(#<?php echo $template_option['loop_template_background_color_top']; ?>), to(#<?php echo $template_option['loop_template_background_color']; ?>));
					/* Opera */
						background: -o-linear-gradient(top, #<?php echo $template_option['loop_template_background_color_top']; ?>, #<?php echo $template_option['loop_template_background_color_top']; ?> 75%, #<?php echo $template_option['loop_template_background_color']; ?> 75%, #<?php echo $template_option['loop_template_background_color']; ?>);
					/* IE */
						filter: progid:DXImageTransform.Microsoft.Gradient(
							StartColorStr='#<?php echo $template_option['loop_template_background_color_top']; ?>', EndColorStr='#<?php echo $template_option['loop_template_background_color']; ?>', GradientType=0);
					<?php } ?>
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_background_image']) ) { ?>
					background-image: url('<?php echo $template_option['loop_template_background_image'] ?>');
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_background_image_repeat']) ) { ?>
					background-repeat: <?php echo $template_option['loop_template_background_image_repeat'].';'; ?>
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_entry_grid']) && $template_option['loop_template_entry_grid'] == 'on' ) { ?>
					float: left;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_height']) || !empty($template_option['loop_template_width']) ) { ?>
					overflow: hidden;
					<?php if ( !empty($template_option['loop_template_height'] ) ) { ?>
						height: <?php echo $template_option['loop_template_height'] ?>px;
					<?php } ?>
					<?php if ( !empty($template_option['loop_template_width']) ) { ?>
						width: <?php echo $template_option['loop_template_width'] ?>;
					<?php } ?>
				<?php } else { ?>
					height: auto;
					width: auto;
					overflow-y: auto;
					overflow-x: hidden;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_corner_radius']) ) { ?>
					border-radius: <?php echo $template_option['loop_template_corner_radius']; ?>px;
					-webkit-border-radius: <?php echo $template_option['loop_template_corner_radius']; ?>px;
					-moz-border-radius: <?php echo $template_option['loop_template_corner_radius']; ?>px;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_border_color']) ) { ?>
					border: 1px solid #<?php echo $template_option['loop_template_border_color']; ?>;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_box_shadow_color']) ) {
					$shadowstyle = "";
					if ( $template_option['loop_template_box_shadow_color'] == "inside" )
						$shadowstyle = "-"; ?>
						 -webkit-box-shadow: <?php echo $shadowstyle; ?>1px 1px 2px 0px #<?php echo $template_option['loop_template_box_shadow_color']; ?>; /* Safari 3+, iOS 4.0.2 - 4.2, Android 2.3+ */
							-moz-box-shadow: <?php echo $shadowstyle; ?>1px 1px 2px 0px #<?php echo $template_option['loop_template_box_shadow_color']; ?>; /* Firefox 3.5 - 3.6 */
								 box-shadow: <?php echo $shadowstyle; ?>1px 1px 2px 0px #<?php echo $template_option['loop_template_box_shadow_color']; ?>; /* Opera 10.5+, IE9+, Firefox 4+, Chrome 6+, iOS 5 */
				<?php } ?>
				margin-bottom: 10px;
			}

			/* post entry's background hover effect */
			.loop-designer div.listposts.<?php echo $template_name; ?>:hover {
				<?php if ( !empty($template_option['loop_template_background_color_hover'])) { ?>
						background: none repeat scroll 0 0 #<?php echo $template_option['loop_template_background_color_hover'] ?>;
				<?php } ?>
			}

			/* featured image styles -> first check if to show the image at all */
			<?php if ( !empty($template_option['loop_template_image_show']) && $template_option['loop_template_image_show'] == 'off' ) : ?>
				.loop-designer div.listposts.<?php echo $template_name; ?> .ld-img {
						display: none;
				}
			<?php else : ?>

				/* the featured image (width, height and image position are not handled here) */
				.loop-designer div.listposts.<?php echo $template_name; ?> img.wp-post-image {
					<?php if ( !empty($template_option['loop_template_image_show']) && $template_option['loop_template_image_show'] == 'off' ) { ?>
						display: none;
					<?php } ?>
					<?php if ( !empty($template_option['loop_template_entry_corner_radius']) ) { ?>
						border-radius: <?php echo $template_option['loop_template_entry_corner_radius']; ?>px;
						-webkit-border-radius: <?php echo $template_option['loop_template_entry_corner_radius']; ?>px;
						-moz-border-radius: <?php echo $template_option['loop_template_entry_corner_radius']; ?>px;
					<?php } ?>
					<?php if ( !empty($template_option['loop_template_image_border_color']) ) { ?>
						border: 1px solid #<?php echo $template_option['loop_template_image_border_color']; ?>;
					<?php } ?>
					<?php if ( !empty($template_option['loop_template_image_box_shadow_color']) ) {
						$shadowstyle = "";
						if ( !empty($template_option['loop_template_image_box_shadow_color']) && $template_option['loop_template_image_box_shadow_color'] == "inside" )
							$shadowstyle = "-"; ?>
							-webkit-box-shadow: <?php echo $shadowstyle; ?>1px 1px 2px 0px #<?php echo $template_option['loop_template_image_box_shadow_color']; ?>; /* Safari 3+, iOS 4.0.2 - 4.2, Android 2.3+ */
							   -moz-box-shadow: <?php echo $shadowstyle; ?>1px 1px 2px 0px #<?php echo $template_option['loop_template_image_box_shadow_color']; ?>; /* Firefox 3.5 - 3.6 */
									box-shadow: <?php echo $shadowstyle; ?>1px 1px 2px 0px #<?php echo $template_option['loop_template_image_box_shadow_color']; ?>; /* Opera 10.5+, IE9+, Firefox 4+, Chrome 6+, iOS 5 */
					<?php } ?>
				}

			<?php endif; ?>

			/* title styles */
			.loop-designer div.listposts.<?php echo $template_name; ?> h3.ld-post-title span.link,
			.loop-designer div.listposts.<?php echo $template_name; ?> h3.ld-post-title a {
				<?php if ( isset($template_option['loop_template_title_show']) && $template_option['loop_template_title_show'] == 'off') { ?>
					display: none;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_title_color'])) { ?>
					color: #<?php echo $template_option['loop_template_title_color']; ?>;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_title_size'])) { ?>
					font-size: <?php echo $template_option['loop_template_title_size']; ?>px;
					line-height: 100%;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_title_font_family']) ) { ?>
					font-family: <?php echo $template_option['loop_template_title_font_family']; ?>;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_title_font_weight'])) { ?>
					font-weight: <?php echo $template_option['loop_template_title_font_weight']; ?>;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_title_font_style'])) { ?>
					font-style: <?php echo $template_option['loop_template_title_font_style']; ?>;
				<?php } ?>
			   <?php if ( !empty($template_option['loop_template_title_text_shadow_color'])) {
					$shadowstyle = "";
					if ( $template_option['loop_template_title_text_shadow_color'] == "inside" )
						$shadowstyle = "-"; ?>
					text-shadow: <?php echo $shadowstyle; ?>1px 1px 1px #<?php echo $template_option['loop_template_title_text_shadow_color']; ?>;
				<?php } ?>
			}

			/* content styles */
			.loop-designer .listposts.<?php echo $template_name; ?> p.ld-post-excerpt,
			.loop-designer .listposts.<?php echo $template_name; ?> p.ld-post-excerpt a,
			.loop-designer .listposts.<?php echo $template_name; ?> p.ld-post-excerpt span.link,
			.loop-designer .listposts.<?php echo $template_name; ?> .ld-readmore {
				<?php if ( isset($template_option['loop_template_content_show']) && $template_option['loop_template_content_show'] == 'off') { ?>
					display: none;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_content_font_size']) ) { ?>
					font-size: <?php echo $template_option['loop_template_content_font_size']; ?>px;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_content_font_family']) ) { ?>
					font-family: <?php echo $template_option['loop_template_content_font_family']; ?>;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_content_font_weight']) ) { ?>
					font-weight: <?php echo $template_option['loop_template_content_font_weight']; ?>;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_content_font_style']) ) { ?>
					font-style: <?php echo $template_option['loop_template_content_font_style']; ?>;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_content_text_shadow_color']) ) {
					$shadowstyle = "";
					if ( !empty($template_option['loop_template_content_text_shadow_style']) && $template_option['loop_template_content_text_shadow_style'] == "inside" )
						$shadowstyle = "-"; ?>
					text-shadow: <?php echo $shadowstyle; ?>1px 1px 1px #<?php echo $template_option['loop_template_content_text_shadow_color']; ?>;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_content_height']) ) { ?>
					height: <?php echo $template_option['loop_template_content_height']; ?>px;
					overflow: hidden;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_image_width']) && ($template_option['loop_template_image_position']) == "posts-img-left-content-right" ) { ?>
					<?php $newmargin = $template_option['loop_template_image_width'] + 15; ?>
					margin-left: <?php echo $newmargin; ?>px;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_image_width']) && ($template_option['loop_template_image_position']) == "posts-img-right-content-left" ) { ?>
					<?php $newmargin = $template_option['loop_template_image_width'] + 15; ?>
					margin-right: <?php echo $newmargin; ?>px;
				<?php } ?>
			}

			<?php if ( !empty($template_option['loop_template_content_font_color']) ) { ?>
				/* content color extra (separate from above, because content color is not for the readmore link ;) */
				.loop-designer .listposts.<?php echo $template_name; ?> p.ld-post-excerpt,
				.loop-designer .listposts.<?php echo $template_name; ?> p.ld-post-excerpt a,
				.loop-designer .listposts.<?php echo $template_name; ?> p.ld-post-excerpt span.link {
						color: #<?php echo $template_option['loop_template_content_font_color']; ?>;
				}
			<?php } ?>

			<?php if ( !empty($template_option['loop_template_content_link_color'] ) ) { ?>
				/* link color extra */
				.loop-designer .listposts.<?php echo $template_name; ?> a,
				.loop-designer .listposts.<?php echo $template_name; ?> span.link,
				.loop-designer .listposts.<?php echo $template_name; ?> .ld-readmore {
						color: #<?php echo $template_option['loop_template_content_link_color']; ?>;
				}
			<?php } ?>

			<?php if ( isset($template_option['loop_template_entry_clickable']) && $template_option['loop_template_entry_clickable'] == true ) { ?>
				/* automatic hover effect on text */
				.loop-designer .listposts.<?php echo $template_name; ?>:hover > h3.ld-post-title span.link,
				.loop-designer .listposts.<?php echo $template_name; ?>:hover > span.link,
				.loop-designer .listposts.<?php echo $template_name; ?>:hover > .ld-readmore span.link,
				.loop-designer .listposts.<?php echo $template_name; ?> a:hover,
				.loop-designer .listposts.<?php echo $template_name; ?> h3.ld-post-title a:hover,
				.loop-designer .listposts.<?php echo $template_name; ?> .ld-readmore a:hover {
					<?php if ( !empty($template_option['loop_template_content_link_color_hover']) ) { ?>
						color: #<?php echo $template_option['loop_template_content_link_color_hover']; ?>;
					<?php } elseif ( !empty($template_option['loop_template_content_font_color']) ) { ?>
						color: #<?php echo $template_option['loop_template_content_font_color']; ?>;
					<?php } ?>
				}
			<?php }?>



			/* font extra */
			.loop-designer .listposts.<?php echo $template_name; ?> p.ld-post-excerpt,
			.loop-designer .listposts.<?php echo $template_name; ?> p.ld-post-excerpt a,
			.loop-designer .listposts.<?php echo $template_name; ?> p a.clickable,
			.loop-designer .listposts.<?php echo $template_name; ?> p span, {
					height: auto;
				<?php if ( !empty($template_option['loop_template_content_font_color']) ) { ?>
					color: #<?php echo $template_option['loop_template_content_font_color']; ?>;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_height']) || !empty($template_option['loop_template_width']) ) { ?>
					overflow: hidden;
				<?php } ?>
				<?php if ( !empty($template_option['loop_template_width']) ) { ?>
					width: <?php echo $template_option['loop_template_width'] ?>;
				<?php } ?>
			}

		</style>

	<?php
	endforeach;
	$tk_loop_designer_template = ob_get_contents();
	ob_end_clean();
	echo  $tk_loop_designer_template;
}
?>