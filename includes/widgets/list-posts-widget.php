<?php

/**
 *  featured posts widget
 *
 * @package x2
 * @since 2.0
 */

class tk_loop_designer_list_posts_widget extends WP_Widget {

    function tk_loop_designer_list_posts_widget() {
          //Constructor
            parent::WP_Widget(false, $name = __( 'TK List Posts', 'tk-loop-designer' ), array(
                'description' => __( 'List Post', 'tk-loop-designer' )
            ));
    }

    function widget($args, $instance) {
        global $post, $template_name;
        extract( $args );
        $tk_loop_designer_template = '';
        $selected_category = !empty($instance['category']) ? esc_attr($instance['category']) : '';
        $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);

        $template_name = !empty($instance['featured_posts_listing_style']) ? $instance['featured_posts_listing_style'] : '';

        $selected_post_type = !empty($instance['featured_posts_post_type']) ? esc_attr($instance['featured_posts_post_type']) : '';
        $selected_taxonomy = !empty($instance['featured_posts_taxonomy']) ? esc_attr($instance['featured_posts_taxonomy']) : '';

        $featured_posts_order_by = !empty($instance['featured_posts_order_by']) ? $instance['featured_posts_order_by'] : '';
        $featured_posts_order = !empty($instance['featured_posts_order']) ? $instance['featured_posts_order'] : '';
        $featured_posts_show_sticky = !empty($instance['featured_posts_show_sticky']) ? $instance['featured_posts_show_sticky'] : '';
        $featured_posts_show_pages_by_id = !empty($instance['featured_posts_show_pages_by_id']) ? $instance['featured_posts_show_pages_by_id'] : '';
        $featured_posts_amount = !empty($instance['featured_posts_amount']) ? $instance['featured_posts_amount'] : '';
        $featured_posts_posts_per_page = !empty($instance['featured_posts_posts_per_page']) ? $instance['featured_posts_posts_per_page'] : '';
        $featured_posts_show_pagination = !empty($instance['featured_posts_show_pagination']) ? $instance['featured_posts_show_pagination'] : '';
        $featured_posts_widget_height = !empty($instance['featured_posts_widget_height']) ? $instance['featured_posts_widget_height'] : '';

        $tk_loop_designer_template .= '<div style="height:'.$featured_posts_widget_height.'px;">';
        $tk_loop_designer_template .= $before_widget;

        if ( ! empty( $title ) )
            $tk_loop_designer_template .=  $before_title . $title . $after_title;

        $atts = array(
            'amount' => $featured_posts_amount,
            'category_name' => $selected_category,
            'img_position' => $template_name,
            'height' => 'auto',
            'page_id' => $featured_posts_show_pages_by_id,
            'post_type' => $selected_post_type,
            'show_sticky' => $featured_posts_show_sticky,
            'show_pagination' => $featured_posts_show_pagination,
            'posts_per_page' => $featured_posts_posts_per_page,
            'featured_id' => $widget_id,
            'orderby' => $featured_posts_order_by,
            'order' => $featured_posts_order,
            'taxonomy' => $selected_taxonomy
        );



        $tk_loop_designer_template .= tk_loop_designer_list_posts($atts,$content = null);
    	$tk_loop_designer_template .= $after_widget;
        $tk_loop_designer_template .= '</div>';

        echo $tk_loop_designer_template;
        wp_reset_query();
    }
    function update($new_instance, $old_instance) {
        //update and save the widget
        return $new_instance;
    }
    function form($instance) {

        //widgetform in backend
        $selected_category = !empty($instance['category']) ? esc_attr($instance['category']) : '';
        $selected_post_type = !empty($instance['featured_posts_post_type']) ? esc_attr($instance['featured_posts_post_type']) : '';
        $selected_taxonomy = !empty($instance['featured_posts_taxonomy']) ? esc_attr($instance['featured_posts_taxonomy']) : '';
        $title = !empty($instance['title']) ? strip_tags($instance['title']) : '';
        $template_name = !empty($instance['featured_posts_listing_style']) ? esc_attr($instance['featured_posts_listing_style']) : '';


        $featured_posts_order_by = !empty($instance['featured_posts_order_by']) ? $instance['featured_posts_order_by'] : '';
        $featured_posts_order = !empty($instance['featured_posts_order']) ? $instance['featured_posts_order'] : '';
        $featured_posts_show_sticky = !empty($instance['featured_posts_show_sticky']) ? $instance['featured_posts_show_sticky'] : '';
        $featured_posts_show_pages_by_id = !empty($instance['featured_posts_show_pages_by_id']) ? $instance['featured_posts_show_pages_by_id'] : '';
        $featured_posts_amount = !empty($instance['featured_posts_amount']) ? $instance['featured_posts_amount'] : '';
        $featured_posts_posts_per_page = !empty($instance['featured_posts_posts_per_page']) ? $instance['featured_posts_posts_per_page'] : '';
        $featured_posts_show_pagination = !empty($instance['featured_posts_show_pagination']) ? $instance['featured_posts_show_pagination'] : '';
        $featured_posts_widget_height = !empty($instance['featured_posts_widget_height']) ? $instance['featured_posts_widget_height'] : '';

       // Get the existing categories and build a simple select dropdown for the user.

        $args=array(
            'public'   => true,
        );
        $output = 'names'; // names or objects, note names is the default
        $operator = 'and'; // 'and' or 'or'
        $post_types=get_post_types($args,$output,$operator);
        foreach ($post_types  as $post_type ) {

            $selected = $selected_post_type === $post_type ? ' selected="selected"' : '';
            $post_type_options[] = '<option value="' . $post_type .'"' . $selected . '>' . $post_type . '</option>';

        }

        $args = array(
            'public'   => true,
            'object_type' => array($selected_post_type)
        );
        $output = 'names'; // or objects
        $operator = 'and'; // 'and' or 'or'
        $taxonomies = get_taxonomies( $args, $output, $operator );
        if ( $taxonomies ) {
            foreach ( $taxonomies  as $taxonomy ) {
                $selected = $selected_taxonomy === $taxonomy ? ' selected="selected"' : '';
                $post_type_taxonomies[] = '<option value="' . $taxonomy .'"' . $selected . '>' . $taxonomy . '</option>';
            }
        }


        $args = array('taxonomy' => $selected_taxonomy, 'echo' => '0','hide_empty' => '0');
        $categories = get_categories($args);
        $cat_options[] = '<option value="">none</option>';
        foreach($categories as $category) {
            $selected = $selected_category === $category->slug ? ' selected="selected"' : '';
            $cat_options[] = '<option value="' . $category->slug .'"' . $selected . '>' . $category->name . '</option>';
        }

       ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:', 'tk-loop-designer' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'List posts template:', 'tk-loop-designer' ); ?></label>
            <select name="<?php echo $this->get_field_name('featured_posts_listing_style'); ?>" id="<?php echo $this->get_field_id('featured_posts_listing_style'); ?>">
				<?php
					$tk_loop_designer_options	= get_option('tk_loop_designer_options');
				?>

				<?php if(is_array($tk_loop_designer_options['template_options'])){
		        	foreach ($tk_loop_designer_options['template_options'] as $tempalte_key => $loop_designer_option) { ?>
		                	<option <?php if($template_name == $tempalte_key){ ?> selected <?php } ?> value="<?php echo $tempalte_key ?>"><?php echo $tempalte_key ?></option>
	        		<?php } ?>
				<?php } ?>

             </select>
           	<p><?php _e( 'Create new templates in the <a href="customize.php#" target="_blank">Customizer</a> in the "TK Loop Designer" tab.', 'tk-loop-designer' ); ?></p>

        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_post_type'); ?>">
                <p><b><?php _e('Post type:', 'tk-loop-designer'); ?></b><br>
                <?php _e('Please save the widget if you change this option', 'tk-loop-designer'); ?></p>
            </label>
            <select id="<?php echo $this->get_field_id('featured_posts_post_type'); ?>" class="widefat" name="<?php echo $this->get_field_name('featured_posts_post_type'); ?>">
                <?php echo implode('', $post_type_options); ?>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_taxonomy'); ?>">
                <p><b><?php _e('Taxonomy:', 'tk-loop-designer'); ?></b><br>
                <?php _e('Please save the widget if you change this option', 'tk-loop-designer'); ?></p>
            </label>
            <select id="<?php echo $this->get_field_id('featured_posts_taxonomy'); ?>" class="widefat" name="<?php echo $this->get_field_name('featured_posts_taxonomy'); ?>">
                <?php echo implode('', $post_type_taxonomies); ?>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('category'); ?>">
                <?php _e('Select a category:', 'tk-loop-designer'); ?>
            </label>
            <select id="<?php echo $this->get_field_id('category'); ?>" class="widefat" name="<?php echo $this->get_field_name('category'); ?>">
                <?php echo implode('', $cat_options); ?>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_order_by'); ?>"><?php _e('Order by:', 'tk-loop-designer'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('featured_posts_order_by'); ?>" name="<?php echo $this->get_field_name('featured_posts_order_by'); ?>" type="text" value="<?php echo esc_attr($featured_posts_order_by); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_order'); ?>"><?php _e('Order:', 'tk-loop-designer'); ?></label><br />
            <select name="<?php echo $this->get_field_name('featured_posts_order'); ?>" id="<?php echo $this->get_field_id('featured_posts_order'); ?>">
                <option <?php if($featured_posts_order == 'ASC'){ ?> selected <?php } ?> value="ASC"><?php _e('Ascending', 'tk-loop-designer'); ?></option>
                <option <?php if($featured_posts_order == 'DESC'){ ?> selected <?php } ?> value="DESC"><?php _e('Descending', 'tk-loop-designer'); ?></option>
             </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_show_sticky'); ?>"><?php _e('Show only sticky posts:', 'tk-loop-designer'); ?></label><br />
            <input type="checkbox" id="<?php echo $this->get_field_id('featured_posts_show_sticky'); ?>" name="<?php echo $this->get_field_name('featured_posts_show_sticky'); ?>" value="on" <?php if(esc_attr($featured_posts_show_sticky) == 'on'){ ?> checked="checked" <?php } ?> /><br />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_show_pages_by_id'); ?>"><?php _e('Page IDs:', 'tk-loop-designer'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('featured_posts_show_pages_by_id'); ?>" name="<?php echo $this->get_field_name('featured_posts_show_pages_by_id'); ?>" type="text" value="<?php echo esc_attr($featured_posts_show_pages_by_id); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_amount'); ?>"><?php _e('Amount of posts:', 'tk-loop-designer'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('featured_posts_amount'); ?>" name="<?php echo $this->get_field_name('featured_posts_amount'); ?>" type="text" value="<?php echo esc_attr($featured_posts_amount); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_posts_per_page'); ?>"><?php _e('Posts per page:', 'tk-loop-designer'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('featured_posts_posts_per_page'); ?>" name="<?php echo $this->get_field_name('featured_posts_posts_per_page'); ?>" type="text" value="<?php echo esc_attr($featured_posts_posts_per_page); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_show_pagination'); ?>">
                <?php _e('Show pagination:', 'tk-loop-designer'); ?>
            </label>
            <select id="<?php echo $this->get_field_id('featured_posts_show_pagination'); ?>" class="widefat" name="<?php echo $this->get_field_name('featured_posts_show_pagination'); ?>">
                <option <?php if($featured_posts_show_pagination == 'show'){ ?> selected <?php } ?> value="show"><?php _e('show', 'tk-loop-designer'); ?></option>
                <option <?php if($featured_posts_show_pagination == 'hide'){ ?> selected <?php } ?> value="hide"><?php _e('hide', 'tk-loop-designer'); ?></option>
                <option <?php if($featured_posts_show_pagination == 'pagenavi'){ ?> selected <?php } ?> value="pagenavi"><?php _e('use wp pagenavi plugin', 'tk-loop-designer'); ?></option>
            </select>
        </p>

         <p>
            <label for="<?php echo $this->get_field_id('featured_posts_widget_height'); ?>"><?php _e("Widget height: <br> for the best result of the jQuery effects we recomend to set a fixed height. We can't do this for you, as we do not know how you configurate the widget.<br> Just type in numbers, without px", 'tk-loop-designer'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('featured_posts_widget_height'); ?>" name="<?php echo $this->get_field_name('featured_posts_widget_height'); ?>" type="text" value="<?php echo esc_attr($featured_posts_widget_height); ?>" />
        </p>

        <?php
    }
}
?>