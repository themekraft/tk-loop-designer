<?php
/**
 * Plugin Name: TK Loop Designer
 * Plugin URI:  http://themekraft.com/store/customize-wordpress-loop-with-tk-loop-designer/
 * Description: Customize Your WordPress Post Listings!
 * Version:     1.2.2
 * Author:      Sven Lehnert
 * Author URI:  http://themekraft.com/members/svenl77/
 * Licence:     GPLv3
 * Text Domain: tk-loop-designer
 * Domain Path: languages/
 */

class TK_Loop_Designer{

	/**
	 * Self Upgrade Values
	 */
	// Base URL to the remote upgrade API server
	public $upgrade_url = 'http://themekraft.com/';

	/**
	 * @var string
	 */
	public $version = '1.2.2';

	/**
	 * @var string
	 */
	public $tk_loop_designer_version_name = 'tk_loop_designer_version';

	/**
	 * PHP 4 constructor
	 *
	 * @package tk-loop-designer
	 * @since 0.9
	 */
	function Loop_Designer() {
		$this->__construct();
	}

	/**
	 * PHP 5 constructor
	 *
	 * @package tk-loop-designer
	 * @since 0.9
	 */
	function __construct() {

		define('TK_LOOP_DESIGNER', '1.2.2' );

		// Run the activation function
		register_activation_hook( __FILE__, array( $this, 'activation' ) );

		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		// Load predefined constants first thing
		add_action( 'tk_loop_designer_init', array( $this, 'load_constants' ), 2 );

		// Includes necessary files
		add_action( 'tk_loop_designer_init', array( $this, 'includes' ), 100, 4 );

		// Includes the necessary js
		add_action('wp_enqueue_scripts', array( $this, 'enqueue_script' ), 2 );
		// add_action('wp_footer', array( $this, 'tk_loop_designer_footer_js' ), 99);

        // Includes the necessary css
         add_action('wp_enqueue_scripts', array( $this, 'enqueue_style' ), 10 );

		 add_action('admin_enqueue_scripts'	, array($this, 'admin_js')		, 2, 1);

		// Add the Widgets
		add_action( 'widgets_init', array( $this, 'tk_register_loop_designer_widget' ), 100, 4 );

		// Let plugins know that tk-loop-designer has started loading
		$this->init_hook();

		// Let other plugins know that tk-loop-designer has finished initializing
		$this->loaded();
		/**
		 * Deletes all data if plugin deactivated
		 */
		register_deactivation_hook( __FILE__, array( $this, 'uninstall' ) );
    }

	function tk_register_loop_designer_widget(){
		register_widget('tk_loop_designer_list_posts_widget');
	}

	/**
	 * defines tk_loop_designer_init  action
	 *
	 * this action fires on WP's init action and provides a way for the rest of tk-loop-designer,
	 * as well as other dependend plugins, to hook into the loading process in an
	 * orderly fashion.
	 *
	 * @package tk-loop-designer
	 * @since 0.9
	 */
	function init_hook() {
		do_action( 'tk_loop_designer_init' );
	}

	/**
	 * defines tk-loop-designer action
	 *
	 * this action tells tk-loop-designer and other plugins that the main initialization process has
	 * finished.
	 *
	 * @package tk-loop-designer
	 * @since 0.9
	 */
	function loaded() {
		do_action( 'tk_loop_designer_loaded' );
	}


	function myplugin_activate() {

	}

	/**
	 * Load the plugin text domain for translation.
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'tk-loop-designer', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}

	/**
	 * defines constants needed throughout the theme.
	 *
	 * these constants can be overridden in bp-custom.php or wp-config.php.
	 *
	 * @package tk-loop-designer
	 * @since 0.9
	 */
	function load_constants() {

		if (!defined('LOOP_DESIGNER_TEMPLATE_PATH'))
			define('LOOP_DESIGNER_TEMPLATE_PATH', dirname(__FILE__) . '/includes/');

		if (!defined('LOOP_DESIGNER_LOOP_TEMPLATES_PATH'))
			define('LOOP_DESIGNER_LOOP_TEMPLATES_PATH', dirname(__FILE__) . '/loop-templates/');

	}

	/**
	 * includes files needed by tk-loop-designer
	 *
	 * @package tk-loop-designer
	 * @since 0.9
	 */
	function includes() {

		if(is_admin()){
			require (dirname(__FILE__) . '/includes/admin/admin.php');
			require (dirname(__FILE__) . '/includes/admin/import-export.php');


			// License Key API Class
			require_once( plugin_dir_path( __FILE__ ) . 'includes/resources/api-manager/classes/class-tk-loop-designer-key-api.php');

			// Plugin Updater Class
			require_once( plugin_dir_path( __FILE__ ) . 'includes/resources/api-manager/classes/class-tk-loop-designer-plugin-update.php');

			// API License Key Registration Form
			require_once( plugin_dir_path( __FILE__ ) . 'includes/admin/license-registration.php');

			// Load update class to update $this plugin from for example toddlahman.com
			$this->load_plugin_self_updater();

		}

		require (dirname(__FILE__) . '/includes/admin/customizer.php');

		require (dirname(__FILE__) . '/includes/the-loop.php');
		require (dirname(__FILE__) . '/includes/helper-functions.php');

		require (dirname(__FILE__) . '/includes/widgets/list-posts-widget.php');
    }




/**
	 * Enqueue the needed JS for the admin screen
	 *
	 * @package
	 * @since 0.1-beta
	 */
	function admin_js($hook_suffix) {

		if( $hook_suffix == 'toplevel_page_tk_loop_designer_options' || $hook_suffix == 'loop-designer_page_tk_loop_designer_import-export' ) {
		add_thickbox();

			wp_enqueue_script('loop_designer_admin_js', plugins_url('includes/admin/js/admin.js', __FILE__));
			wp_enqueue_script('tk_loop_designer_zendesk_js', '//assets.zendesk.com/external/zenbox/v2.6/zenbox.js');
			wp_enqueue_style('tk_loop_designer_zendesk_css', '//assets.zendesk.com/external/zenbox/v2.6/zenbox.css' );

	    }

	}



	###  js
 	function enqueue_script() {
 	   wp_enqueue_script('loop-designer-js', plugins_url('includes/js/loop-designer.js', __FILE__), array('jquery') );
    }

    ### add css
    function enqueue_style() {

		wp_enqueue_style('loop-designer', plugins_url('includes/css/loop-designer.css', __FILE__) );

	}


	/**
	 * Check for software updates
	 */
	public function load_plugin_self_updater() {
		$options = get_option( 'tk_loop_designer_license_manager' );

		// upgrade url must also be chaned in classes/class-tk-loop-designer-key-api.php
		$upgrade_url = $this->upgrade_url; // URL to access the Update API Manager.
		$plugin_name = 'tk_loop_designer'; // same as plugin slug. if a theme use a theme name like 'twentyeleven'
		$product_id = get_option( 'tk_loop_designer_product_id' ); // Software Title
		$api_key = $options['api_key']; // API License Key
		$activation_email = $options['activation_email']; // License Email
		$renew_license_url = 'http://themekraft.com/my-account/'; // URL to renew a license
		$instance = get_option( 'tk_loop_designer_instance' ); // Instance ID (unique to each blog activation)
		$domain = site_url(); // blog domain name
		$software_version = get_option( $this->tk_loop_designer_version_name ); // The software version
		$plugin_or_theme = 'plugin'; // 'theme' or 'plugin'

		new TK_Loop_Designer_Plugin_Update_API_Check( $upgrade_url, $plugin_name, $product_id, $api_key, $activation_email, $renew_license_url, $instance, $domain, $software_version, $plugin_or_theme );
	}


	/**
	 * Generate the default data arrays
	 */
	public function activation() {

		$global_options = array(
			'api_key' 			=> '',
			'activation_email' 	=> '',
					);

		update_option( 'tk_loop_designer_license_manager', $global_options );

		// Password Management Class
		require_once( plugin_dir_path( __FILE__ ) . 'includes/resources/api-manager/classes/class-tk-loop-designer-passwords.php');

		$tk_loop_designer_password_management = new TK_Loop_Designer_Password_Management();

		// Generate a unique installation $instance id
		$instance = $tk_loop_designer_password_management->generate_password( 12, false );

		$single_options = array(
			'tk_loop_designer_product_id' 			=> 'TK Loop Designer',
			'tk_loop_designer_instance' 				=> $instance,
			'tk_loop_designer_deactivate_checkbox' 	=> 'on',
			'tk_loop_designer_activated' 				=> 'Deactivated',
			);

		foreach ( $single_options as $key => $value ) {
			update_option( $key, $value );
		}

		$curr_ver = get_option( $this->tk_loop_designer_version_name );

		// checks if the current plugin version is lower than the version being installed
		if ( version_compare( $this->version, $curr_ver, '>' ) ) {
			// update the version
			update_option( $this->tk_loop_designer_version_name, $this->version );
		}
		tk_loop_designer_load_default_templates();

	}

	/**
	 * Deletes all data if plugin deactivated
	 * @return void
	 */
	public function uninstall() {
		global $wpdb, $blog_id;

		$this->license_key_deactivation();

		// Remove options
		if ( is_multisite() ) {

			switch_to_blog( $blog_id );

			foreach ( array(
					'tk_loop_designer_license_manager',
					'tk_loop_designer_product_id',
					'tk_loop_designer_instance',
					'tk_loop_designer_deactivate_checkbox',
					'tk_loop_designer_activated',
					'tk_loop_designer_version'
					) as $option) {

					delete_option( $option );

					}

			restore_current_blog();

		} else {

			foreach ( array(
					'tk_loop_designer_license_manager',
					'tk_loop_designer_product_id',
					'tk_loop_designer_instance',
					'tk_loop_designer_deactivate_checkbox',
					'tk_loop_designer_activated'
					) as $option) {

					delete_option( $option );

					}

		}

	}

	/**
	 * Deactivates the license on the API server
	 * @return void
	 */
	public function license_key_deactivation() {

		$tk_loop_designer_key = new TK_Loop_Designer_Key();

		$activation_status = get_option( 'tk_loop_designer_activated' );

		$default_options = get_option( 'tk_loop_designer_license_manager' );

		$api_email = $default_options['activation_email'];
		$api_key = $default_options['api_key'];

		$args = array(
			'email' => $api_email,
			'licence_key' => $api_key,
			);

		if ( $activation_status == 'Activated' && $api_key != '' && $api_email != '' ) {
			$tk_loop_designer_key->deactivate( $args ); // reset license key activation
		}
	}



}

$GLOBALS['tk_Loop_designer'] = new TK_Loop_Designer();
?>